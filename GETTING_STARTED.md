# GETTING STARTED

## ABOUT THE API

Features in this API:

- Retrieve the most related occupations for an education, by providing education title and free text description for the education as input.
- Retrieve the most related occupations for a specific education in SUSA-Navet.
- Match educations for an occupation. The matching is done with the most common competencies for the occupation.
- Search/filter educations in SUSA-Navet (See: <https://utbildningsguiden.skolverket.se/om-oss/susa---data-om-utbildningar>).

## TERMS OF USE

- The API and its source code is Open source and free to use and free to clone and modify.
- This API is not intended to be an advanced search engine for educations in SUSA-navet, so the search part should be
  treated as hopefully good enough.
- The matching endpoints matches related educations and occupations (and vice verse) and the correctness depends on
  data quality. Also, it has not been possible to test all educations as they are constantly changing. It is also possible
  to match from free text, which leads to endless test cases. So the matching should be treated as possible relations
  between educations and occupations (and vice verse) and not as precise mappings. In other words, there is no guarantee
  that the related occupations for an education is what a student will be able to work as after studying.

## DATA MODEL

- Swagger: <https://jobed-connect-api.jobtechdev.se/>

## LIFECYCLE AND VERSION HANDLING

- The first version was released in production environment in spring 2023.
- We use <https://semver.org/>

## CONTACTS

- Jobtech Forum: <https://forum.jobtechdev.se/>
