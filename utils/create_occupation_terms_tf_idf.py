import json
import logging
import os

import jmespath
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler
from educationapi.common.opensearch_common import create_os_client

# See https://medium.com/analytics-vidhya/demonstrating-calculation-of-tf-idf-from-sklearn-4f9526e7e78b

log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

os_client = create_os_client()
occupations_handler = EnrichedOccupationsHandler(os_client)



def prepare_docs():
    log.info("Preparing docs..")
    docs = []
    unique_terms = set()
    all_enriched_occupations = occupations_handler.get_all_enriched_occupations()
    occupation_counter = 0
    len_all_enriched_occupations = len(all_enriched_occupations)
    for occupation_id in all_enriched_occupations:
        occupation_counter += 1
        occupation = occupations_handler.find_occupation_by_concept_id(occupation_id)
        occupation_label = jmespath.search('label', occupation)
        log.info(f"Adding terms for {occupation_counter}/{len_all_enriched_occupations}: {occupation_label}, concept_id: {occupation_id}")

        competence_terms = jmespath.search('enriched_candidates.competencies', occupation)
        if competence_terms:
            for term in competence_terms:
                unique_terms.add(term)
            concatenated_terms = ' '.join(competence_terms)
            docs.append(concatenated_terms)
    log.info("Done preparing docs!")
    return docs, list(unique_terms)


def calculate_idf():
    docs, unique_terms = prepare_docs()
    log.info("Calculating IDF..")
    #
    # concept_id_dentist = 'TdvW_S8R_CFH'
    # concept_id_bus_driver = 'Qq5v_QZ3_EE9'
    # concept_id_police_assistant = 'rv8W_E8i_aMo'
    # concept_id_systems_developer = 'fg7B_yov_smw'
    # concept_id_truck_driver = 'GQSf_fnq_kjF'
    # docs = []
    # docs.append(get_concatenated_competence_terms_for_occupation(concept_id_dentist))
    # docs.append(get_concatenated_competence_terms_for_occupation(concept_id_bus_driver))
    # docs.append(get_concatenated_competence_terms_for_occupation(concept_id_police_assistant))
    # docs.append(get_concatenated_competence_terms_for_occupation(concept_id_systems_developer))
    # docs.append(get_concatenated_competence_terms_for_occupation(concept_id_truck_driver))
    cv = CountVectorizer(ngram_range=(1, 2), vocabulary=unique_terms)
    word_count_vector = cv.fit_transform(docs)
    # tf = pd.DataFrame(word_count_vector.toarray(), columns=cv.get_feature_names_out())
    # print(tf)
    tfidf_transformer = TfidfTransformer()
    X = tfidf_transformer.fit_transform(word_count_vector)
    idf = pd.DataFrame({'feature_name': cv.get_feature_names_out(), 'idf_weights': tfidf_transformer.idf_})
    idf = idf.sort_values('idf_weights')
    idf.reset_index()
    # print(idf)
    #
    tf_idf = pd.DataFrame(X.toarray(), columns=cv.get_feature_names_out())
    # print(tf_idf)
    column_names = tf_idf.columns.values.tolist()
    # print(idf_mean_dict)
    log.info("Done calculating IDF!")
    return tf_idf, column_names


def write_mean_to_json_file(tf_idf, column_names):

    output_filepath = f'{currentdir}../educationapi/resources/enriched_occupations_tf_idf_mean.json'
    log.info(f"Writing result to: {output_filepath}")
    idf_mean_dict = {}
    for column_name in column_names:
        column_mean = tf_idf[column_name].mean()
        idf_mean_dict[column_name] = column_mean
    idf_mean_dict_sorted =  {k: v for k, v in sorted(idf_mean_dict.items(), key=lambda item: item[1], reverse=True)}

    with open(output_filepath, 'w', encoding='utf-8') as fp:
        json.dump(idf_mean_dict_sorted, fp, sort_keys=False, indent=4)


if __name__ == '__main__':
    tf_idf, column_names = calculate_idf()
    write_mean_to_json_file(tf_idf, column_names)





