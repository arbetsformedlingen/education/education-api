import json
import logging
import os

import jmespath

from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler
from educationapi.common.opensearch_common import create_os_client

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

os_client = create_os_client()
enriched_occupations_handler = EnrichedOccupationsHandler(os_client)

all_enriched_occupations = enriched_occupations_handler.get_all_enriched_occupations()

log.info(f'len(all_enriched_occupations): {len(all_enriched_occupations)}')



def get_occupation_details(occupation_id):
    '''
    Gets an enriched occupation with for example enriched competencies and jobtitles.
    :param occupation_id: unique taxonomy conceptId
    '''
    if not occupation_id or occupation_id not in all_enriched_occupations:
        return None

    enriched_occupation = enriched_occupations_handler.find_occupation_by_concept_id(occupation_id)
    # log.info(json.dumps(enriched_occupation, indent=4))

    return enriched_occupation

def get_occupation_by_label(label):
    return next((item for item in all_enriched_occupations.values() if item["label"].lower() == label.lower()), None)


def save_labels_and_competencies_count_for_all_occupations():


    all_enriched = list(all_enriched_occupations.values())

    enriched_occupations = all_enriched

    occupations_to_save = fetch_occupation_to_save(enriched_occupations)

    log.debug(json.dumps(occupations_to_save, indent=4))

    sorted_occupations_to_save = sorted(occupations_to_save, key=lambda d: d['unique_competencies_count'], reverse=True)

    for counter, occ_to_save in enumerate(sorted_occupations_to_save):
        occ_to_save['item_at_index'] = counter

    with open(currentdir + 'occupations_unique_candidates_count.json', 'w', encoding='utf-8') as f:
        json.dump(sorted_occupations_to_save, f, ensure_ascii=False, indent=4)


def fetch_occupation_to_save(enriched_occupations):
    occupations_to_save = []
    for counter, occupation_plain in enumerate(enriched_occupations):
        occupation_detailed = get_occupation_details(occupation_plain['concept_id'])
        unique_competencies = set(jmespath.search('enriched_candidates.competencies', occupation_detailed))
        unique_occupations = set(jmespath.search('enriched_candidates.occupations', occupation_detailed))
        occupation_to_save = {
            "label": jmespath.search('label', occupation_detailed),
            "concept_id": jmespath.search('concept_id', occupation_detailed),
            "unique_competencies_count": len(unique_competencies),
            "unique_occupations_count": len(unique_occupations),
            'enriched_ads_count': jmespath.search('enriched_ads_count', occupation_detailed)
        }
        occupations_to_save.append(occupation_to_save)
        if counter > 0 and counter % 100 == 0:
            log.info(f'Got details for {counter} occupations')
    return occupations_to_save

save_labels_and_competencies_count_for_all_occupations()