# README - utils

### Create automatic mappings between jobtitle and Taxonomy occupation
**Note:** The script should be executed after a new version of the  
synonym dictionary ([JobAd Enrichments](https://jobad-enrichments-api.jobtechdev.se/))
has been released or after changing taxonomy version.

Run the file ```create_jobtitle_occupation_mappings.py``` to create the file  
\educationapi\resources\jobtitle_occupation_tax_v21_mappings_thresh_0.5_ratio_500.csv  
that is used when mapping a jobtitle to the corresponding Taxonomy occupation.
 

### Create TF/IDF mean value for boosted queries (match educations from occupation or jobtitle)
**Note:** The script should be executed after a new version of the  
synonym dictionary ([JobAd Enrichments](https://jobad-enrichments-api.jobtechdev.se/))
has been released or after changing taxonomy version.

Run the file ```create_occupation_terms_tf_idf.py``` to create the file
\educationapi\resources\enriched_occupations_tf_idf_mean.json  
The file is used when creating boost values in the query for matching
educations from an occupation or a jobtitle.
