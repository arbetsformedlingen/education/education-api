import csv
import json
import logging
import os

import jmespath

from educationapi import settings
from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler
from educationapi.common.opensearch_common import create_os_client
from educationapi.common.synonym_dictionary import SynonymDictionary
from educationapi.matchers.educations_matcher import EducationsMatcher

"""
Creates a .csv-file with mapping from a jobtitle to the most likely Taxonomy occupation.

Example from created file:
jobtitle;occupation_label;occupation_concept_id
.net-utvecklare;Systemutvecklare/Programmerare;fg7B_yov_smw
affischuppsättare;Affischör/Affischuppsättare;7MTC_RFi_5qp
akutsjuksköterska;Akutsjuksköterska/Sjuksköterska, akutmottagning;Pi5N_5NB_7BY
"""

JOBTITLE_PERCENT_TRESHOLD = 0.5
RATIO_THRESHOLD = 500

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + " log level activated")

currentdir = os.path.dirname(os.path.realpath(__file__)) + "/"

FILEPATH_BLACKLIST_OCCUPATION_MAPPINGS = (
    currentdir + "resources/blacklist_occupation_mappings.csv"
)


def search_relevant_occupation(jobtitle, os_client, synonym_dictionary):
    found_occupation = None

    # Translate jobtitle to concept_term
    concepts = synonym_dictionary.get_concepts(jobtitle)
    if len(concepts) > 0:
        concept_term = concepts[0]["concept"].lower()
        # log.info('Found concept_term: %s' % concept_term)

        source_excludes_fields = ["occupation.enriched_candidates.*"]

        query = {
            "query": {
                "bool": {
                    "should": [
                        {
                            "match_phrase": {
                                "occupation.enriched_candidates.occupations": concept_term
                            }
                        }
                    ],
                    "minimum_should_match": "1",
                }
            }
        }
        # log.info(json.dumps(query))

        occupations_results = os_client.search(
            index=settings.ES_ENRICHED_OCCUPATIONS_ALIAS,
            body=query,
            size=1,
            track_total_hits=False,
            _source_excludes=source_excludes_fields,
        )

        # log.debug('occupations_results: %s' % json.dumps(occupations_results))

        found_occupation = jmespath.search(
            "hits.hits[0]._source.occupation", occupations_results
        )
        # log.debug('Found occupation: %s' % found_occupation)
    return found_occupation


def create_mappings(os_client):
    log.info("Creating mappings..")
    jobtitle_occupation_mappings = {}
    synonym_dictionary = SynonymDictionary(os_client)
    enriched_occupations_handler = EnrichedOccupationsHandler(os_client)
    educations_matcher = EducationsMatcher(os_client)

    synonym_terms = synonym_dictionary.get_terms_by_type("YRKE")

    blacklist_occupation_mappings = _get_blacklist_occupation_mappings_from_file()

    occupation_unique_concepts = sorted(
        list(set([term_obj["concept"].lower() for term_obj in synonym_terms]))
    )

    # occupation_unique_concepts = occupation_unique_concepts[0:100]
    # occupation_unique_concepts = ['affärsutvecklingsassistent']
    # occupation_unique_concepts = ['a-kassehandläggare']
    # print(occupation_unique_concepts[0:100])

    for jobtitle in occupation_unique_concepts:
        if jobtitle not in jobtitle_occupation_mappings:
            relevant_occupation = search_relevant_occupation(
                jobtitle, os_client, synonym_dictionary
            )
            rel_occupation_label = jmespath.search("label", relevant_occupation)
            rel_occupation_concept_id = jmespath.search(
                "concept_id", relevant_occupation
            )
            rel_occupation_enriched_ads_count = jmespath.search(
                "enriched_ads_count", relevant_occupation
            )
            # log.info(f'Jobtitle: {jobtitle} - Found occupation: {rel_occupation_label}')

            percent_for_jobtitle = (
                enriched_occupations_handler.get_percent_for_jobtitle_in_occupation(
                    rel_occupation_concept_id, jobtitle
                )
            )

            log.info(
                f"Jobtitle: {jobtitle} - Relevant occupation: {rel_occupation_label} - percent for jobtitle: {percent_for_jobtitle}"
            )
            if percent_for_jobtitle and rel_occupation_enriched_ads_count:
                ratio = percent_for_jobtitle * rel_occupation_enriched_ads_count
            else:
                ratio = 0

            if (
                not percent_for_jobtitle > JOBTITLE_PERCENT_TRESHOLD
                and ratio < RATIO_THRESHOLD
            ):
                rel_occupation_label = None
                rel_occupation_concept_id = None

            if is_blacklisted_occupation_mapping(
                jobtitle, rel_occupation_label, blacklist_occupation_mappings
            ):
                rel_occupation_label = None
                rel_occupation_concept_id = None

            jobtitle_occupation_mappings[jobtitle] = {
                "jobtitle": jobtitle,
                "occupation_label": rel_occupation_label,
                "occupation_concept_id": rel_occupation_concept_id,
                "percent_for_jobtitle": round(percent_for_jobtitle, 2),
                "occupation_enriched_ads_count": rel_occupation_enriched_ads_count,
                # "is_manual_mapping": False
            }

    manual_jobtitle_occupation_mappings = (
        educations_matcher._get_manual_jobtitle_mappings_from_taxonomy()
    )

    for jobtitle, occupation in manual_jobtitle_occupation_mappings.items():
        occupation_label = jmespath.search("occupation_label", occupation)
        occupation_concept_id = jmespath.search("occupation_concept_id", occupation)
        jobtitle_occupation_mappings[jobtitle] = {
            "jobtitle": jobtitle,
            "occupation_label": occupation_label,
            "occupation_concept_id": occupation_concept_id,
            "percent_for_jobtitle": 0,
            "occupation_enriched_ads_count": 0,
            # "is_manual_mapping": True
        }

    if jobtitle_occupation_mappings:
        jobtitle_occupation_mappings_list = list(jobtitle_occupation_mappings.values())
        jobtitle_occupation_mappings_list = sorted(
            jobtitle_occupation_mappings_list, key=lambda d: d["jobtitle"]
        )

        keys_for_column_names = [
            key for key in jobtitle_occupation_mappings_list[0].keys()
        ]
        # print(keys_for_column_names)
        mappings_filepath = f"{currentdir}../educationapi/resources/jobtitle_occupation_tax_v21_mappings_thresh_{JOBTITLE_PERCENT_TRESHOLD}_ratio_{RATIO_THRESHOLD}.csv"
        with open(mappings_filepath, "w", newline="", encoding="utf-8") as output_file:
            dict_writer = csv.DictWriter(
                output_file, keys_for_column_names, delimiter=";"
            )
            dict_writer.writeheader()
            dict_writer.writerows(jobtitle_occupation_mappings_list)


def _load_json_file(filepath):
    log.info("Loading json from file: %s" % filepath)
    with open(filepath, "r", encoding="utf-8") as file:
        data = json.load(file)
        return data


def _get_blacklist_occupation_mappings_from_file():
    blacklist_occupation_mappings = []
    with open(FILEPATH_BLACKLIST_OCCUPATION_MAPPINGS, encoding="utf-8") as f:
        DictReader_obj = csv.DictReader(f, delimiter=";")
        for item in DictReader_obj:
            for key in item.keys():
                # Set all values to lowercase to easy comparison later.
                item[key] = item[key].lower()
            blacklist_occupation_mappings.append(item)
    return blacklist_occupation_mappings


def is_blacklisted_occupation_mapping(
    jobtitle, occupation_label, blacklisted_occupation_mappings
):
    jobtitle = jobtitle.lower()
    occupation_label = occupation_label.lower() if occupation_label else ""
    is_blacklisted = next(
        (
            item
            for item in blacklisted_occupation_mappings
            if item["jobtitle"].lower() == jobtitle
            and item["occupation_label"].lower() == occupation_label
        ),
        None,
    )

    return is_blacklisted is not None


if __name__ == "__main__":
    os_client = create_os_client()

    create_mappings(os_client)

# print(_get_blacklist_occupation_mappings_from_file())
# print(is_blacklisted_occupation_mapping('akutläkare', 'at-läkare', blacklist_occupation_mappings))


# a-kassehandläggare
# # agile coach
# # agil coach
# # .net-utvecklare

# print(search_relevant_occupation('1:a tandsköterska'))
# print(search_relevant_occupation('agile coach'))


# https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html#/
# query MyQuery {   concepts(type: "occupation-name") {     alternative_labels     id     preferred_label     type   } }
