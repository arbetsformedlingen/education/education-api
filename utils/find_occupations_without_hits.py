import csv
import logging
import os

import jmespath

from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler
from educationapi.common.opensearch_common import create_os_client
from educationapi.common.synonym_dictionary import SynonymDictionary
from educationapi.matchers.occupations_matcher import OccupationsMatcher

'''
Creates a .csv-file with number of matched items for a jobtitle

Example from created file:
jobtitle;result_size
'''

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def match_all_jobtitles():
    synonym_dictionary = SynonymDictionary()

    synonym_terms = synonym_dictionary.get_terms_by_type('YRKE')

    unique_jobtitle_concepts = sorted(list(set([term_obj['concept'].lower() for term_obj in synonym_terms])))

    print(f"len(unique_jobtitle_concepts): {len(unique_jobtitle_concepts)}")

    os_client = create_os_client()
    enriched_occupations_handler = EnrichedOccupationsHandler(os_client)

    occupations_matcher = OccupationsMatcher(os_client, enriched_occupations_handler)

    items_to_save = []

    for jobtitle in unique_jobtitle_concepts:
        match_result = occupations_matcher.match_occupations_by_text('', input_headline=jobtitle, limit=1,
                                                                     include_metadata=False)

        first_hit_occupation_label = jmespath.search("related_occupations[0].occupation_label", match_result)
        hits_total = jmespath.search("hits_total", match_result)

        item_to_save = {
            "jobtitle": jobtitle,
            "first_hit_occupation_label": first_hit_occupation_label,
            "hits_total": hits_total
        }
        print(item_to_save)
        items_to_save.append(item_to_save)

    items_to_save = sorted(items_to_save, key=lambda d: d['jobtitle'])

    keys_for_column_names = [key for key in items_to_save[0].keys()]
    with open(currentdir + f'occupations_hits.csv', 'w', newline='', encoding='utf-8') as output_file:
        dict_writer = csv.DictWriter(output_file, keys_for_column_names, delimiter=';')
        dict_writer.writeheader()
        dict_writer.writerows(items_to_save)

    zero_hits_size = len(list(filter(lambda d: d['hits_total'] == 0, items_to_save)))
    total_jobtitles_size = len(items_to_save)
    more_than_zero_hits_size = total_jobtitles_size - zero_hits_size
    print(f'Total jobtitles: {total_jobtitles_size}, Total number of zero hits: {zero_hits_size}, Number of more than zero hits: {more_than_zero_hits_size}')


match_all_jobtitles()
