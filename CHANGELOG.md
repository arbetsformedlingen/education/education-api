# Changelog

## 1.4.0 (May 2024)
* Update API version for release. Changes made in merge-education: [Added start and end date to event summary] (https://gitlab.com/arbetsformedlingen/education/education-merge-educations/-/issues/11)

## 1.3.2 (March 2024)
* Resolved case sensitivity issue with search text for autocomplete endpoint.
* [Filter out occupations that lead to unexpected search results for education] (https://gitlab.com/arbetsformedlingen/education/education-api/-/issues/48)

## 1.3 1 (February 2024)
* Adds län (region) as filter when searching educations
* Adds län (region) as filter when matching educations from a jobtitle
* Fixes problem with jobtitle autocomplete when typing 'x' (which returned all jobtitles instead of none)

## 1.3.0 (November 2023)
* Adjusts API to the new dataset/index 'enriched-occupations' that only contains Taxonomy occupations with version 21.

## 1.2.4 (November 2023)
* Fixes logging for Openshift (stacktraces in the same post instead of multiple posts because of line endings)

## 1.2.3 (October 2023)
* Change to Debian-12
* Return http 400 instead of 500 when offset too high
* Updates versions in requirements

## 1.2.2 (August 2023)
* Adds files for load testing

## 1.2.1 (April 2023)
* Includes occupation-group data in response from /v1/jobtitles/autocomplete

## 1.1.0 (February 2023)
* Adds support for multiple values as input for education type and education form.

## 1.0.0 (February 2023)

* Initial release

## 1.4.1 (November 2024)
* Increase version form 1.4.0 to 1.4.1 for easy way to verify that the upcoming switch from AWS to Onprem was successful.
