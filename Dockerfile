FROM docker.io/library/python:3.10.15-slim-bookworm


ENV FLASK_APP=educationapi \
    TZ=Europe/Stockholm \
    PROC_NR=4 \
    HARAKIRI=180

# uwsgi won't run without libexpat.so.1
RUN apt-get -y update && \
    apt-get install --no-install-recommends -y \
    libexpat1 \
    git && \
    apt-get install --no-install-recommends -y libexpat1 && \
    rm -rf /var/lib/apt/lists/*

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /data
COPY . /data
RUN pip install --upgrade pip && pip install -r requirements.txt

EXPOSE 5000

CMD  uwsgi --http :5000 --ini uwsgi.ini
