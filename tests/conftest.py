import pytest

from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler
from educationapi.common.opensearch_common import create_os_client


@pytest.fixture(scope="session")
def os_client():
    return create_os_client()


@pytest.fixture(scope="session")
def enriched_occupations_handler(os_client):
    return EnrichedOccupationsHandler(os_client)
