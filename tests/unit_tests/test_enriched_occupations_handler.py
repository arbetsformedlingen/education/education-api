import pytest  # noqa: F401
from unittest.mock import Mock, call, patch

from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler


def test_EnrichedOccupationsHandler():
    mock_os_client = Mock()
    enriched_occupations_handler = EnrichedOccupationsHandler(mock_os_client)

    assert enriched_occupations_handler.log is not None
    assert enriched_occupations_handler.enriched_occupations_alias is not None
    assert enriched_occupations_handler.os_client is mock_os_client


@patch("educationapi.common.enriched_occupations_handler.scan")
def test_EnrichedOccupationsHandler_get_all_enriched_occupations(mock_scan):
    # Arrange
    mock_os_client = Mock()
    mock_scan.return_value = [
        {"_source": {"entry_without_id": "this_entry_has_no_id"}},
        {
            "_source": {
                "id": "some_id",
                "occupation": {
                    "occupation_details": "for_test_concept_id",
                },
            }
        },
    ]

    # Act
    enriched_occupations_handler = EnrichedOccupationsHandler(mock_os_client)

    returned_enriched_occupations = (
        enriched_occupations_handler.get_all_enriched_occupations()
    )

    # Assert
    mock_scan.assert_called_once()

    mock_scan.assert_called_with(
        mock_os_client,
        {
            "_source": {
                "excludes": [
                    "occupation.enriched_candidates.*",
                    "occupation.relevant_enriched_candidates.*",
                ]
            },
            "query": {"match_all": {}},
        },
        index="enriched-occupations",
        size=1000,
        request_timeout=60,
    )

    expected_enriched_occupations = {
        "some_id": {"occupation_details": "for_test_concept_id"}
    }

    assert returned_enriched_occupations == expected_enriched_occupations
    assert (
        enriched_occupations_handler._all_enriched_occupations
        == expected_enriched_occupations
    )
