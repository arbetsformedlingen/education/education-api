from datetime import datetime
import pytest
from unittest.mock import Mock

from educationapi.matchers.educations_matcher import EducationsMatcher

@pytest.fixture(scope="module")
def educations_matcher():
    # Adding the required arguments for EducationsMatcher
    os_client = Mock()  # or your actual os_client
    taxonomy_client = Mock()  # or your actual taxonomy_client
    educations_matcher = EducationsMatcher(os_client, taxonomy_client)
    return educations_matcher


def test_create_match_educations_query(educations_matcher):
    competencies_term_boost = {}
    occupations_term_boost = {}
    education_type = None
    education_form = None
    municipality_code = None
    region_code = None
    distance = True

    result = educations_matcher.create_match_educations_query(
        competencies_term_boost, occupations_term_boost, education_type, education_form, municipality_code, region_code, distance
    )

    current_date = datetime.now().strftime('%Y-%m-%d')

    assert 'must' in result['query']['bool']
    assert 'term' in result['query']['bool']['must'][0]
    assert result['query']['bool']['must'][0]['term']['eventSummary.distance'] is True

    must_list = result['query']['bool']['must']
    assert len(must_list) > 1, "Expected more elements in the 'must' list"

    should_node = must_list[1]['bool']['should']
    assert len(should_node) > 0, "Expected elements in the 'should' node"

    start_condition = should_node[0]['bool']['must'][0]['range']['eventSummary.executions.start']['gt']
    assert start_condition == current_date

    end_condition = should_node[1]['bool']['must'][0]['range']['eventSummary.executions.end']['gt']
    assert end_condition == current_date
