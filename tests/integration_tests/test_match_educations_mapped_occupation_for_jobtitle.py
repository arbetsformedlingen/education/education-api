import logging
import os

import pytest

from educationapi.matchers.educations_matcher import EducationsMatcher

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def educations_matcher(os_client, enriched_occupations_handler):
    educations_matcher = EducationsMatcher(os_client, enriched_occupations_handler)
    return educations_matcher


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_mapped_occupation_for_jobtitle_2(educations_matcher):
    result = educations_matcher._get_mapped_occupation_for_jobtitle("ögonsjuksköterska")
    taxonomy_id_eye_nurse = "nvZM_5xs_qd9"
    assert result["occupation_concept_id"] == taxonomy_id_eye_nurse


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_mapped_occupation_for_non_mapped_jobtitle(educations_matcher):
    result = educations_matcher._get_mapped_occupation_for_jobtitle("1:a tandsköterska")
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_mapped_occupation_for_mapped_but_not_enriched_occupation(
    educations_matcher,
):
    result = educations_matcher._get_mapped_occupation_for_jobtitle("agil coach")
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_mapped_occupation_for_invalid_jobtitle(educations_matcher):
    result = educations_matcher._get_mapped_occupation_for_jobtitle("invalidjobtitle")
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_jobtitle_mappings_from_taxonomy(educations_matcher):
    result = educations_matcher._get_manual_jobtitle_mappings_from_taxonomy()

    assert len(result.items()) > 0


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
