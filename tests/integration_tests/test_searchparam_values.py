import logging
import os

import pytest

from educationapi.common.searchparam_values import SearchparamValues

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def searchparam_values(os_client):
    return SearchparamValues(os_client)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_education_forms(searchparam_values):
    result = searchparam_values.get_education_forms()
    # log.info(json.dumps(result))
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_education_types(searchparam_values):
    result = searchparam_values.get_education_types()
    # log.info(json.dumps(result))
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_municipalities_from_taxonomy(searchparam_values):
    result = searchparam_values._get_municipalities_from_taxonomy()
    # log.info(json.dumps(result))
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_municipality_codes(searchparam_values):
    result = searchparam_values.get_municipality_codes()
    # log.info(json.dumps(result))
    assert len(result) > 0


def test_get_regions_from_taxonomy(searchparam_values):
    result = searchparam_values._get_regions_from_taxonomy()
    # log.info(json.dumps(result))
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_region_codes(searchparam_values):
    result = searchparam_values.get_region_codes()
    # log.info(json.dumps(result))
    assert len(result) > 0


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
