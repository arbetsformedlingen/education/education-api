import logging
import os

import jmespath
import pytest
from educationapi.common.model.search_educations_query_params import (
    SearchEducationsQueryParams,
)
from educationapi.common.opensearch_store import OpensearchStore
from educationapi.common.pagination_params import PaginationParams

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def opensearch_store(os_client):
    opensearch_store = OpensearchStore(os_client)
    return opensearch_store


# @pytest.mark.skip(reason="Temporarily disabled")
def test_search_educations_by_municipality(opensearch_store):
    search_educations_query_params = SearchEducationsQueryParams()
    search_educations_query_params.education_form = ["högskoleutbildning"]
    search_educations_query_params.municipality_code = "1480"

    pagination_params = PaginationParams()
    pagination_params.offset = 0
    pagination_params.limit = 10

    result = opensearch_store.freetext_search(
        search_educations_query_params, pagination_params, True
    )

    # log.info(json.dumps(result))
    assert result
    assert jmespath.search("hits", result) > 0


def test_search_educations_by_region(opensearch_store):
    search_educations_query_params = SearchEducationsQueryParams()
    search_educations_query_params.education_form = ["högskoleutbildning"]
    search_educations_query_params.region_code = "03"

    pagination_params = PaginationParams()
    pagination_params.offset = 0
    pagination_params.limit = 10

    result = opensearch_store.freetext_search(
        search_educations_query_params, pagination_params, True
    )

    assert result
    assert jmespath.search("hits", result) > 0


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
