import ndjson
import os
import pytest

from educationapi.matchers.occupations_matcher import OccupationsMatcher


@pytest.fixture(scope="module")
def occupations_matcher(os_client, enriched_occupations_handler):
    occupations_matcher = OccupationsMatcher(os_client, enriched_occupations_handler)
    return occupations_matcher


def occupation_labels_from_occupations(occupations):
    occupation_labels = []
    if isinstance(occupations, list):
        for occupation in occupations:
            if occupation.get("occupation_label"):
                occupation_labels.append(occupation["occupation_label"])
    return occupation_labels


def data_loader(file):
    # Data loader that marks tests to skip if exclude is set in data file
    filename = os.path.join(os.path.dirname(__file__), file)
    with open(filename, "r", encoding="utf-8") as f:
        return [
            pytest.param(
                item["education_headline"],
                item["education_text"],
                item["expected_occupations"],
                marks=pytest.mark.skipif(
                    item.get("exclude", False),
                    reason="Excluded in data file.",
                ),
            )
            for item in ndjson.load(f)
        ]


@pytest.mark.parametrize(
    "headline,text,expected_occupations",
    data_loader("./../resources/freetext_educations_with_matches.jsonl"),
)
def test_first_matched_occupation(
    headline, text, expected_occupations, occupations_matcher
):
    # For each education text verify that the first actual matched occupation is exactly the same as the expected first occupation.
    expected_occupation_labels = occupation_labels_from_occupations(
        expected_occupations
    )

    match = occupations_matcher.match_occupations_by_text(text, headline)
    actual_related_occupations = match.get("related_occupations", None)
    actual_occupation_labels = occupation_labels_from_occupations(
        actual_related_occupations
    )

    assert actual_occupation_labels[0] == expected_occupation_labels[0]


@pytest.mark.parametrize(
    "headline,text,expected_occupations",
    data_loader("./../resources/freetext_educations_with_matches.jsonl"),
)
def test_second_matched_occupation_in_expected_list(
    headline, text, expected_occupations, occupations_matcher
):
    # For each education text verify that the second actual matched occupation is in the expected list of matched occupations.
    expected_occupation_labels = occupation_labels_from_occupations(
        expected_occupations
    )

    match = occupations_matcher.match_occupations_by_text(text, headline)
    actual_related_occupations = match.get("related_occupations", None)
    actual_occupation_labels = occupation_labels_from_occupations(
        actual_related_occupations
    )

    assert len(actual_occupation_labels) > 1
    assert (
        actual_occupation_labels[1] in expected_occupation_labels
    ), f"Could not find {actual_occupation_labels[1]} in {actual_occupation_labels}."


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
