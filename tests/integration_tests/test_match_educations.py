import logging
import os
import string
import sys

import jmespath
import pytest
import pandas as pd

from educationapi.matchers.educations_matcher import EducationsMatcher

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + "/"

# File with jobtitles that will be blacklisted in autocomplete endpoints
FILEPATH_EDUCATION_MATCHER_BLACKLIST = f"{currentdir}../../educationapi/resources/blacklist_educations_matcher_jobtitles.txt"

# File with jobtitles that will be blacklisted in education matcher endpoints
FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST = f"{currentdir}../../educationapi/resources/blacklist_educations_matcher_occupations.csv"

occupation_ids = ["TdvW_S8R_CFH", "ofiS_5F2_YmV", "bXNH_MNX_dUR"]
jobtitles = ["sjuksköterska", "HR-assistent", "växtodlingsrådgivare"]


@pytest.fixture(scope="module")
def educations_matcher(os_client, enriched_occupations_handler):
    educations_matcher = EducationsMatcher(os_client, enriched_occupations_handler)
    return educations_matcher


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id(educations_matcher, occupation_id):
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id, limit=2
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_positive_offset(
    educations_matcher, occupation_id
):
    result_no_offset = educations_matcher.match_educations_by_occupation_id(
        occupation_id, limit=10
    )
    result_offset = educations_matcher.match_educations_by_occupation_id(
        occupation_id, limit=10, offset=10
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result_no_offset) > 0
    assert jmespath.search("hits_total", result_offset) > 0
    result_no_offset_hits = jmespath.search("hits", result_no_offset)
    result_offset_hits = jmespath.search("hits", result_offset)
    assert result_offset_hits[0] != result_no_offset_hits[0]


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_negative_offset(
    educations_matcher, occupation_id
):
    result_offset = educations_matcher.match_educations_by_occupation_id(
        occupation_id, limit=10, offset=-10
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result_offset) > 0


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_education_type(
    educations_matcher, occupation_id
):
    education_type = ["program"]
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id, education_type=education_type, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0

    for hit in jmespath.search("hits", result):
        assert jmespath.search("education_type", hit) == education_type[0]


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_education_form(
    educations_matcher, occupation_id
):
    education_form = ["högskoleutbildning"]
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id, education_form=education_form, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0
    for hit in jmespath.search("hits", result):
        assert jmespath.search("education_form", hit) == education_form[0]


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_municipality_code(
    educations_matcher, occupation_id
):
    municipality_code_stockholm = "0180"
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id,
        municipality_code=municipality_code_stockholm,
        limit=10,
        include_metadata=False,
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


@pytest.mark.parametrize("occupation_id", occupation_ids)
def test_match_educations_by_occupation_id_with_region_code(
    educations_matcher, occupation_id
):
    region_code_stockholm = "01"
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id,
        region_code=region_code_stockholm,
        limit=10,
        include_metadata=False,
    )
    assert jmespath.search("hits_total", result) > 0


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_distance(educations_matcher, occupation_id):
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id, distance=True, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_occupation_id_include_metadata(
    educations_matcher, occupation_id
):
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id, limit=2, include_metadata=True
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("metadata", result)
    assert jmespath.search("metadata.occupation_enriched_ads_count", result)
    assert jmespath.search("metadata.competencies_term_boost", result)
    assert jmespath.search("metadata.occupations_term_boost", result)


@pytest.mark.parametrize("occupation_id", occupation_ids)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_metadata_term_boost_sort_order(
    educations_matcher, occupation_id
):
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_id, limit=1, include_metadata=True
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("metadata", result)
    # assert jmespath.search('metadata.occupation_enriched_ads_count', result)
    competencies_term_boost = jmespath.search(
        "metadata.competencies_term_boost", result
    )
    occupations_term_boost = jmespath.search("metadata.occupations_term_boost", result)
    assert competencies_term_boost
    assert occupations_term_boost
    last_seen = sys.maxsize
    for boost_value in competencies_term_boost.values():
        assert boost_value <= last_seen
        last_seen = boost_value

    last_seen = sys.maxsize
    for boost_value in occupations_term_boost.values():
        assert boost_value <= last_seen
        last_seen = boost_value


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_invalid_occupation_id(educations_matcher):
    result = educations_matcher.match_educations_by_occupation_id(
        "NonExistingId", limit=1
    )

    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_empty_id(educations_matcher):
    result = educations_matcher.match_educations_by_occupation_id(None, limit=1)

    assert result is None


def test_match_educations_by_bogus_id(educations_matcher):
    result = educations_matcher.match_educations_by_occupation_id("bogus_id")

    assert result is None


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle(educations_matcher, jobtitle):
    result = educations_matcher.match_educations_by_jobtitle(jobtitle, limit=2)
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_problematic(educations_matcher):
    jobtitle = "polisassistent"
    result = educations_matcher.match_educations_by_jobtitle(jobtitle, limit=10)
    # log.info(json.dumps(result))
    education_titles = jmespath.search("hits[*].education_title", result)
    assert jmespath.search("hits_total", result) > 0

    assert "Data Center Technician (DCO)" not in education_titles


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_max_amount_searchparams(educations_matcher):
    jobtitle = "Sjuksköterska"
    education_type = ["program"]
    education_form = ["högskoleutbildning"]
    municipality_code_stockholm = "0180"
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle,
        education_type=education_type,
        education_form=education_form,
        municipality_code=municipality_code_stockholm,
        limit=2,
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


# # @pytest.mark.skip(reason="Temporarily disabled")
# def test_match_educations_by_all_jobtitles(educations_matcher, jobtitle):
#     jobtitle_occupation_mappings = educations_matcher._get_jobtitle_occupation_mappings()
#
#     # TODO: Kolla samtliga yrkestitlar och se vilka som får 0 träffar.
#     for key, value in jobtitle_occupation_mappings.items():
#         jobtitle = key
#         print(jobtitle)
#     # result = educations_matcher.match_educations_by_jobtitle(jobtitle, limit=2)
#     # # log.info(json.dumps(result))
#     # assert jmespath.search('hits_total', result) > 0


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_include_metadata(educations_matcher, jobtitle):
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle, limit=2, include_metadata=True
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("metadata", result)
    assert jmespath.search("metadata.occupation_enriched_ads_count", result)
    assert jmespath.search("metadata.competencies_term_boost", result)
    assert jmespath.search("metadata.occupations_term_boost", result)


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_positive_offset(educations_matcher, jobtitle):
    result_no_offset = educations_matcher.match_educations_by_jobtitle(
        jobtitle, limit=10
    )
    result_offset = educations_matcher.match_educations_by_jobtitle(
        jobtitle, limit=10, offset=10
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result_no_offset) > 0
    assert jmespath.search("hits_total", result_offset) > 0
    result_no_offset_hits = jmespath.search("hits", result_no_offset)
    result_offset_hits = jmespath.search("hits", result_offset)
    assert result_offset_hits[0] != result_no_offset_hits[0]


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_education_type(educations_matcher, jobtitle):
    education_type = ["program"]
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle, education_type=education_type, limit=20
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0
    for hit in jmespath.search("hits", result):
        assert jmespath.search("education_type", hit) == education_type[0]


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_education_form(educations_matcher, jobtitle):
    education_form = ["högskoleutbildning"]
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle, education_form=education_form, limit=20
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0
    for hit in jmespath.search("hits", result):
        assert jmespath.search("education_form", hit) == education_form[0]


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_municipality_code(educations_matcher, jobtitle):
    municipality_code_stockholm = "0180"
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle, municipality_code=municipality_code_stockholm, limit=20
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_with_region_code(educations_matcher, jobtitle):
    region_code_stockholm = "01"
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle, region_code=region_code_stockholm, limit=20
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


@pytest.mark.parametrize("jobtitle", jobtitles)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_jobtitle_distance(educations_matcher, jobtitle):
    result = educations_matcher.match_educations_by_jobtitle(
        jobtitle, distance=True, limit=20
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_educations_by_invalid_jobtitle(educations_matcher):
    result = educations_matcher.match_educations_by_jobtitle(
        "NonExistingJobTitle", limit=2
    )
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_all_typeahead_jobtitle_terms(educations_matcher):
    result = educations_matcher._get_all_valid_autocomplete_terms_and_occupation_ids()
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_jobtitles_default_size(educations_matcher):
    search_word = "sju"
    excepted_result_size = 10
    result = educations_matcher.autocomplete_jobtitles(search_word)
    # log.info(result)
    assert len(result) == excepted_result_size
    for item in result:
        assert item["jobtitle"].startswith(search_word)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_jobtitles_tes(educations_matcher):
    search_word = "tes"
    excepted_result_size = 10
    result = educations_matcher.autocomplete_jobtitles(search_word)
    # log.info(result)
    # assert len(result) == excepted_result_size
    for item in result:
        assert item["jobtitle"].startswith(search_word)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_5_jobtitles(educations_matcher):
    search_word = "sju"
    excepted_result_size = 5
    result = educations_matcher.autocomplete_jobtitles(
        search_word, size=excepted_result_size
    )
    # log.info(result)
    assert len(result) == excepted_result_size
    for item in result:
        assert item["jobtitle"].startswith(search_word)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_50_jobtitles(educations_matcher):
    search_word = "s"
    excepted_result_size = 50
    result = educations_matcher.autocomplete_jobtitles(
        search_word, size=excepted_result_size
    )
    # log.info(result)
    assert len(result) == excepted_result_size
    for item in result:
        assert item["jobtitle"].startswith(search_word)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_ensure_occupation_group_data(educations_matcher):
    search_word = "s"
    excepted_result_size = 50
    result = educations_matcher.autocomplete_jobtitles(
        search_word, size=excepted_result_size
    )
    # log.info(result)
    assert len(result) == excepted_result_size
    for item in result:
        assert "jobtitle" in item
        assert "occupation_id" in item
        assert "occupation_label" in item
        assert "occupation_group" in item
        occupation_group = item["occupation_group"]
        assert "occupation_group_label" in occupation_group
        assert "concept_taxonomy_id" in occupation_group
        assert "ssyk" in occupation_group

        assert occupation_group[
            "occupation_group_label"
        ], f'Could not find a value for jobtitle {item["jobtitle"]}'

        # assert item['jobtitle'].startswith(search_word)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_number_of_empty_occupation_groups_for_all_searchwords(
    educations_matcher,
):
    unique_occupations = set()
    unique_jobtitles = set()
    unique_occupations_missing_ssyk = set()
    unique_jobtitles_missing_ssyk = set()

    autocomplete_words = educations_matcher.autocomplete.words
    for search_word in autocomplete_words:
        excepted_result_size = 50
        result = educations_matcher.autocomplete_jobtitles(
            search_word, size=excepted_result_size
        )
        for item in result:
            assert "jobtitle" in item
            assert "occupation_label" in item
            assert "occupation_group" in item
            occupation_group = item["occupation_group"]
            assert "occupation_group_label" in occupation_group
            assert "concept_taxonomy_id" in occupation_group
            assert "ssyk" in occupation_group
            assert len(occupation_group["ssyk"]) > 0

            unique_occupations.add(item["occupation_label"])
            unique_jobtitles.add(item["jobtitle"])

            if not occupation_group["occupation_group_label"]:
                unique_occupations_missing_ssyk.add(item["occupation_label"])
                unique_jobtitles_missing_ssyk.add(item["jobtitle"])

    log.info(f"len(autocomplete_words): {len(autocomplete_words)}")
    log.info(f"len(unique_occupations): {len(unique_occupations)}")
    log.info(f"len(unique_jobtitles): {len(unique_jobtitles)}")
    log.info(
        f"len(unique_occupations_missing_ssyk): {len(unique_occupations_missing_ssyk)}"
    )
    log.info(
        f"len(unique_jobtitles_missing_ssyk): {len(unique_jobtitles_missing_ssyk)}"
    )
    assert len(unique_occupations) > 0
    assert len(unique_jobtitles) > 0
    assert len(unique_occupations_missing_ssyk) == 0
    assert len(unique_jobtitles_missing_ssyk) == 0

    # diff = [term for term in autocomplete_words if term not in unique_jobtitles]
    # log.info(f'diff: {diff}')


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_searchterms_with_no_matching_jobtitles(educations_matcher):
    unique_occupations = set()
    unique_jobtitles = set()
    autocomplete_words = [
        "c#-utvecklare",
        "c++-utvecklare",
        "cnc- operatör",
        "lånechef",
        "lånehandläggare",
        "vd assistent",
        "vd sekreterare",
        "vfx artist",
        "värdpersonal",
    ]

    for search_word in autocomplete_words:
        excepted_result_size = 50
        result = educations_matcher.autocomplete_jobtitles(
            search_word, size=excepted_result_size
        )
        for item in result:
            assert "jobtitle" in item
            assert "occupation_label" in item
            assert "occupation_group" in item
            occupation_group = item["occupation_group"]
            assert "occupation_group_label" in occupation_group
            assert "concept_taxonomy_id" in occupation_group
            assert "ssyk" in occupation_group
            assert len(occupation_group["ssyk"]) > 0

            unique_occupations.add(item["occupation_label"])
            unique_jobtitles.add(item["jobtitle"])

    log.info(f"len(autocomplete_words): {len(autocomplete_words)}")

    # Note: Allow minor differences since fast_autocomplete package can give results like:
    # searchterm: 'cnc- operatör' result: 'cnc-operatör'
    # searchterm: 'vfx artist' result: 'vfx-artist'
    diff = [
        term
        for term in autocomplete_words
        if term not in unique_jobtitles
        and term.replace(" ", "-") not in unique_jobtitles
        and term.replace(" ", "") not in unique_jobtitles
    ]
    assert len(diff) == 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_check_if_empty_occupation_groups(educations_matcher):
    autocomplete_words = educations_matcher.autocomplete.words
    with open(FILEPATH_EDUCATION_MATCHER_BLACKLIST, encoding="utf-8") as stoplistfile:
        blacklist_items = stoplistfile.readlines()
    blacklist_items = [x.strip() for x in blacklist_items]
    for search_word in autocomplete_words:
        if search_word not in blacklist_items:
            log.info(f"Checking autocomplete for searchword: {search_word}")
            excepted_result_size = 10
            result = educations_matcher.autocomplete_jobtitles(
                search_word, size=excepted_result_size
            )
            assert len(result) > 0, "Result should be greater than 0"
            for item in result:
                assert "jobtitle" in item
                assert "occupation_label" in item
                assert "occupation_group" in item
                occupation_group = item["occupation_group"]
                assert "occupation_group_label" in occupation_group
                assert "concept_taxonomy_id" in occupation_group
                assert "ssyk" in occupation_group

                assert occupation_group["occupation_group_label"]


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_searchchar_with_matching_jobtitles(educations_matcher):
    autocomplete_chars = list(string.ascii_lowercase)
    autocomplete_chars.extend(["å", "ä", "ö"])
    autocomplete_chars.remove("x")

    for search_word in autocomplete_chars:
        unique_jobtitles = set()

        excepted_result_size = 10
        result = educations_matcher.autocomplete_jobtitles(
            search_word, size=excepted_result_size
        )
        for item in result:
            assert "jobtitle" in item
            unique_jobtitles.add(item["jobtitle"])

        log.info(f"len(autocomplete_chars): {len(autocomplete_chars)}")

        assert len(unique_jobtitles) > 0

        for jobtitle in unique_jobtitles:
            assert jobtitle.startswith(
                search_word
            ), f'"{jobtitle}" does not start with "{search_word}"'


# @pytest.mark.skip(reason="Temporarily disabled")
def test_autocomplete_searchchar_with_no_matching_jobtitles(educations_matcher):
    autocomplete_words = ["x"]

    for search_word in autocomplete_words:
        excepted_result_size = 10
        result = educations_matcher.autocomplete_jobtitles(
            search_word, size=excepted_result_size
        )

        assert len(result) == 0


def test_autocomplete_blacklist(educations_matcher):
    with open(FILEPATH_EDUCATION_MATCHER_BLACKLIST, encoding="utf-8") as stoplistfile:
        blacklist_items = stoplistfile.readlines()
    blacklist_items = [x.strip() for x in blacklist_items]
    search_word = blacklist_items[0]
    result = educations_matcher.autocomplete_jobtitles(search_word)
    assert len(result) == 0


def test_match_educations_by_occupation_id_blacklist(educations_matcher):
    df_blacklist = pd.read_csv(
        FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST,
        usecols=["occupation_label", "occupation_concept_id"],
        sep=";",
        keep_default_na=False,
    )
    occupation_taxonomy_concept_id = df_blacklist["occupation_concept_id"].iloc[0]
    result = educations_matcher.match_educations_by_occupation_id(
        occupation_taxonomy_concept_id
    )
    assert result is None


def test_match_educations_by_jobtitle_blacklist(educations_matcher):
    df_blacklist = pd.read_csv(
        FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST,
        usecols=["occupation_label", "occupation_concept_id"],
        sep=";",
        keep_default_na=False,
    )
    jobtitle = df_blacklist["occupation_label"].iloc[0]
    result = educations_matcher.match_educations_by_jobtitle(jobtitle.lower())
    assert result is None


def test_mapped_occupation_for_jobtitle_blacklist(educations_matcher):
    df_blacklist = pd.read_csv(
        FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST,
        usecols=["occupation_label", "occupation_concept_id"],
        sep=";",
        keep_default_na=False,
    )
    jobtitle = df_blacklist["occupation_label"].iloc[0]
    result = educations_matcher._get_mapped_occupation_for_jobtitle(jobtitle.lower())
    assert result is None


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
