import logging
import os

import pytest

from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def enriched_occupations_handler(os_client):
    return EnrichedOccupationsHandler(os_client)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_all_enriched_occupations(enriched_occupations_handler):
    result = enriched_occupations_handler.get_all_enriched_occupations()
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def find_occupation_by_concept_id(enriched_occupations_handler):
    concept_id_dentist = "TdvW_S8R_CFH"
    result = enriched_occupations_handler.find_occupation_by_concept_id(
        concept_id_dentist
    )
    # log.info(json.dumps(result))
    assert len(result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_calculate_term_frequency(enriched_occupations_handler):
    result = enriched_occupations_handler.calculate_term_frequency(
        ["java", "java", "agile", "python", "javascript", "c#"]
    )

    # log.info(result)
    assert len(result) > 0
    assert find_index_of_item(result, "term", "java") < find_index_of_item(
        result, "term", "agile"
    )


def find_index_of_item(dicts, key_to_check, value_to_find):
    return next(
        (i for i, item in enumerate(dicts) if item[key_to_check] == value_to_find), None
    )


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
