import logging
import os

import jmespath
import ndjson
import pytest

from educationapi.matchers.occupations_matcher import OccupationsMatcher

currentdir = os.path.dirname(os.path.realpath(__file__)) + "/"

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def merged_educations():
    return load_ndjson_file(
        currentdir + "../resources/merged_and_enriched_educations_sample.jsonl"
    )


@pytest.fixture(scope="module")
def occupations_matcher(os_client, enriched_occupations_handler):
    occupations_matcher = OccupationsMatcher(os_client, enriched_occupations_handler)
    return occupations_matcher


@pytest.mark.parametrize("occupation_id", ["p17k_znk_osi"])
# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_occupation_details_without_metadata(occupations_matcher, occupation_id):
    result = occupations_matcher.get_occupation_details(
        occupation_id, include_metadata=False
    )
    # print(json.dumps(result))
    assert jmespath.search("id", result) == occupation_id


@pytest.mark.parametrize("occupation_id", ["p17k_znk_osi"])
# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_occupation_details_with_metadata(occupations_matcher, occupation_id):
    result = occupations_matcher.get_occupation_details(
        occupation_id, include_metadata=True
    )
    # print(json.dumps(result))
    assert jmespath.search("id", result) == occupation_id
    assert "metadata" in result
    metadata_node = jmespath.search("metadata", result)
    assert "enriched_ads_count" in metadata_node
    assert "enriched_ads_total_count" in metadata_node
    assert "enriched_ads_percent_of_total" in metadata_node
    assert "enriched_candidates_term_frequency" in metadata_node
    term_freq_node = jmespath.search(
        "enriched_candidates_term_frequency", metadata_node
    )
    assert "competencies" in term_freq_node
    assert "geos" in term_freq_node
    assert "occupations" in term_freq_node
    assert "traits" in term_freq_node
    enriched_competencies = jmespath.search("competencies", term_freq_node)
    assert len(enriched_competencies) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_occupation_details_invalid_id(occupations_matcher):
    result = occupations_matcher.get_occupation_details(
        "invalid-id-12345", include_metadata=True
    )
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_susa_education(merged_educations, occupations_matcher):
    education_id = "i.uoh.uu.msj1y.p3500.20222"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result)
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("identified_keywords_for_input", result)
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    assert len(jmespath.search("identified_keywords_for_input.occupations", result)) > 0
    assert len(jmespath.search("related_occupations", result)) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_susa_education_gymnasium(
    merged_educations, occupations_matcher
):
    education_id = "i.sv.ek001"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result)
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("identified_keywords_for_input", result)
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    # assert len(jmespath.search('identified_keywords_for_input.occupations', result)) > 0
    related_occupations = jmespath.search("related_occupations", result)
    assert len(related_occupations) > 0
    for rel_occupation in related_occupations:
        assert not jmespath.search("occupation_label", rel_occupation).startswith(
            "lärare"
        )


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_id_enriched_occupations(
    merged_educations, occupations_matcher
):
    education_id = "i.myh.7743"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))

    assert jmespath.search("hits_total", result)
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("identified_keywords_for_input", result)
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    assert len(jmespath.search("identified_keywords_for_input.occupations", result)) > 0
    assert len(jmespath.search("related_occupations", result)) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_education_code(merged_educations, occupations_matcher):
    education_code = "YH00550"
    test_education = get_test_education_by_code(education_code, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, include_metadata=False
    )

    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result)
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("identified_keywords_for_input", result)
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    assert len(jmespath.search("identified_keywords_for_input.occupations", result)) > 0
    assert len(jmespath.search("related_occupations", result)) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_education_code_include_metadata(
    merged_educations, occupations_matcher
):
    education_code = "YH00550"
    test_education = get_test_education_by_code(education_code, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, include_metadata=True
    )
    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result)
    assert jmespath.search("hits_total", result) > 0
    assert jmespath.search("identified_keywords_for_input", result)
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    assert len(jmespath.search("identified_keywords_for_input.occupations", result)) > 0
    assert len(jmespath.search("related_occupations", result)) > 0

    first_hit = jmespath.search("related_occupations[0]", result)
    assert "metadata" in first_hit
    metadata_node = jmespath.search("metadata", first_hit)
    assert "enriched_ads_count" in metadata_node
    assert "enriched_ads_total_count" in metadata_node
    assert "enriched_ads_percent_of_total" in metadata_node
    assert "match_score" in metadata_node

    assert "enriched_candidates_term_frequency" not in metadata_node


@pytest.mark.parametrize("education_code", ["NON_VALID_EDUCATION_CODE_88888888"])
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_non_valid_education_code(
    occupations_matcher, education_code
):
    result = occupations_matcher.match_occupations_by_education_code(
        education_code, limit=10, include_metadata=False
    )

    assert not result


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_id_positive_offset(
    merged_educations, occupations_matcher
):
    # TODO: Bytt till id/utbildning som ger fler än 10 träffar.
    # education_id = 'i.fbr.60994.352675'
    education_id = "i.myh.7605"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result_no_offset = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, include_metadata=False
    )
    result_offset = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, offset=10, include_metadata=False
    )

    assert jmespath.search("hits_total", result_no_offset) > 0
    assert jmespath.search("hits_total", result_offset) > 0
    result_no_offset_hits = jmespath.search("related_occupations", result_no_offset)
    result_offset_hits = jmespath.search("related_occupations", result_offset)
    assert result_offset_hits[0] != result_no_offset_hits[0]


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_id_negative_offset(
    merged_educations, occupations_matcher
):
    education_id = "i.fbr.60994.352675"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result_offset = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=10, offset=-10, include_metadata=False
    )

    assert jmespath.search("hits_total", result_offset) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_invalid_id(occupations_matcher):
    result = occupations_matcher.match_occupations_by_id(
        "invalid-id-12345", limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_empty_id(occupations_matcher):
    result = occupations_matcher.match_occupations_by_id(
        None, limit=10, include_metadata=False
    )
    # log.info(json.dumps(result))
    assert result is None


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_id_include_metadata(
    merged_educations, occupations_matcher
):
    education_id = "i.myh.7908"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=1, include_metadata=True
    )

    # log.info(json.dumps(result))

    assert jmespath.search("hits_total", result) > 0
    assert len(jmespath.search("related_occupations", result)) > 0
    first_hit = jmespath.search("related_occupations[0]", result)
    assert "metadata" in first_hit
    metadata_node = jmespath.search("metadata", first_hit)
    assert "enriched_ads_count" in metadata_node
    assert "enriched_ads_total_count" in metadata_node
    assert "enriched_ads_percent_of_total" in metadata_node
    assert "match_score" in metadata_node

    assert "enriched_candidates_term_frequency" not in metadata_node


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_high_limit(merged_educations, occupations_matcher):
    education_id = "i.myh.7908"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result = occupations_matcher.match_occupation_by_susa_education(
        test_education, limit=1000000, include_metadata=False
    )

    # log.info(json.dumps(result))
    assert jmespath.search("hits_total", result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_create_input_for_match_occupation_by_susa_education(
    occupations_matcher, merged_educations
):
    education_id = "i.uoh.kau.vgssk.70156.20202"
    test_education = get_test_education_by_id(education_id, merged_educations)
    result = occupations_matcher.create_input_for_match_occupation_by_susa_education(
        test_education
    )

    assert jmespath.search("candidates_headline", result)
    assert jmespath.search("candidates_text", result)
    assert jmespath.search("candidates_headline.competencies", result)
    assert jmespath.search("candidates_text.competencies", result)
    assert len(jmespath.search("candidates_headline.competencies", result)) > 0
    assert len(jmespath.search("candidates_text.competencies", result)) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_create_input_for_match_occupations_by_text(occupations_matcher):
    input_headline = "Praktik, juridik med inriktning folkrätt"
    input_text = """forskning
                    forskningsmetoder
                    franska
                    internationell rätt
                    juridik
                    layout
                    litteraturvetenskap
                    träning
                    ämneskunskaper"""
    result = occupations_matcher.create_input_from_education_text(
        input_headline, input_text
    )

    assert jmespath.search("candidates_headline", result)
    assert jmespath.search("candidates_text", result)
    assert jmespath.search("candidates_headline.competencies", result)
    assert jmespath.search("candidates_text.competencies", result)
    assert len(jmespath.search("candidates_headline.competencies", result)) > 0
    assert len(jmespath.search("candidates_text.competencies", result)) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_projektledare(occupations_matcher):
    input_headline = "projektledare"
    input_text = """projektarbete"""
    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )

    related_occupations = jmespath.search("related_occupations", result)
    related_occupations_terms = [
        term["occupation_label"].lower() for term in related_occupations
    ]

    assert len(related_occupations_terms) > 0
    first_hit = related_occupations_terms[0]
    assert first_hit.startswith("projektledare")


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_competencies_in_headline_and_text(
    occupations_matcher,
):
    input_headline = "hälsa, miljö och samhälle kandidatprogram i folkhälsovetenskap"
    input_text = """folkhälsovetenskap
                    hälsa
                    kandidatprogram
                    miljö
                    hälsovetenskap"""
    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )

    assert jmespath.search("hits_total", result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_min_limit_enriched_ads_count(occupations_matcher):
    input_headline = "team leader"
    input_text = """kundservice
                    engelska
                    svenska
                    körkort"""
    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=True
    )

    assert jmespath.search("hits_total", result) > 0

    related_occupations = jmespath.search("related_occupations", result)
    for related_occupation in related_occupations:
        enriched_ads_count = jmespath.search(
            "metadata.enriched_ads_count", related_occupation
        )
        occupation_label = jmespath.search("occupation_label", related_occupation)
        assert (
            enriched_ads_count
            >= OccupationsMatcher.MINIMUM_ENRICHED_ADS_FOR_OCCUPATION_COUNT
        ), f"{occupation_label} has too few enriched ads and should not be in the result"


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_competencies_in_only_headline(occupations_matcher):
    input_headline = "hälsa, miljö och samhälle kandidatprogram i folkhälsovetenskap"
    input_text = ""
    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )

    assert jmespath.search("hits_total", result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_not_wanted_occupations_in_result(
    occupations_matcher,
):
    input_headline = "Regional miljögeovetenskap"
    input_text = """genusvetenskap
                    identifiering
                    konsultföretag
                    lokalområdet
                    vetenskapsteori"""
    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )

    related_occupations = jmespath.search("related_occupations", result)
    related_occupations_terms = [
        term["occupation_label"].lower() for term in related_occupations
    ]

    for not_wanted_occupation in occupations_matcher.blacklist_result_terms_dict[
        "occupations"
    ]:
        assert not not_wanted_occupation in related_occupations_terms


@pytest.mark.parametrize(
    "input_text",
    [
        "Fastighetsförvaltare är ett av de mest populära yrkesvalen och efterfrågan är extremt stor. Några av de kvalificerade kompetenser som du lär dig är affärsmannaskap, ekonomisk förvaltning och kundkontakt.Vad gör en fastighetsförvaltare?Som fastighetsförvaltare är det du som har ansvaret för fastigheten. Bland annat är det din uppgift att se till så att hyresgästerna trivs och att fastighetens ekonomi uppfyller de uppställda målen. Arbetet kan se olika ut beroende på vilken typ av fastighet du är ansvarig för. Du kan enbart ha hand om vissa delar av fastighetsförvaltningen eller ha ett totalansvar.För dig som gillar ansvarSom fastighetsförvaltare ansvarar du för planering av reparationer och underhåll samt tillsyn av städning och att uppvärmningen fungerar. Du ser även till att lagar följs och upphandlar konsulter och entreprenörer samt i vissa fall ansvarar för egen personal.Yrkesrollen medför en stor mängd kontakter med hyresgäster, myndigheter och entreprenörer. Arbetsuppgifterna förändras ständigt vilket gör att kompetenskraven ökar och efterfrågan på utbildade fastighetsförvaltare är mycket hög.Nära samarbete med branschenJohn Ericsson Institutet har ett tätt samarbete med några av branschens främsta föreläsare och lärare. Det sitter även flera välkända företag med i utbildningens ledningsgrupp.Denna utbildning är framtagen i samarbete med bland annat Einar Mattsson AB, Vasakronan, ByggVesta, Gunnar Karlsén, Riba AB, Mi Casa, Jernhusen, WSP, Värmex, Newsec, Akademiska Hus och många fler engagerade på olika sätt.Kostnadsfri och CSN-berättigadDet kostar inget att gå utbildningen men eventuella utgifter för läroböcker kan förekomma.  Utbildning berättigar till både lån och stöd från Centrala Studiestödsnämnden (CSN). Se filmen om studiemedelVi följer riktlinjerna från FHMUtbildningen genomförs utifrån de riktlinjer och råd som gäller från Folkhälsomyndigheten.Efter examen kommer du bland annat att kunna arbeta som:FastighetsförvaltareOmrådesförvaltare, fastighetFastighetsförmedlareBiträdande fastighetsförvaltareUthyrare, fastighet",
        "Under utbildningen så lär du dig java, javascript, python, c# och testdriven utveckling och du kommer sedan att kunna jobba som Systemutvecklare.",
        "<div><p>Under utbildningen så lär du dig java, javascript, python, c# och testdriven utveckling och du kommer sedan att kunna jobba som Systemutvecklare.</p></div>",
    ],
)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text(occupations_matcher, input_text):
    result = occupations_matcher.match_occupations_by_text(
        input_text, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_no_result_should_return_identified_keywords(
    occupations_matcher,
):
    input_text = """java python"""
    result = occupations_matcher.match_occupations_by_text(
        input_text, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) == 0
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    assert (
        len(jmespath.search("identified_keywords_for_input.occupations", result)) == 0
    )


@pytest.mark.parametrize("input_text", ["java. Jobba som systemutvecklare"])
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_should_return_identified_keywords(
    occupations_matcher, input_text
):
    result = occupations_matcher.match_occupations_by_text(
        input_text, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0
    assert (
        len(jmespath.search("identified_keywords_for_input.competencies", result)) > 0
    )
    assert len(jmespath.search("identified_keywords_for_input.occupations", result)) > 0


@pytest.mark.parametrize(
    "input_text",
    [
        "Under utbildningen så lär du dig java, javascript, python, c# och testdriven utveckling."
    ],
)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_include_metadata(occupations_matcher, input_text):
    result = occupations_matcher.match_occupations_by_text(
        input_text, limit=10, include_metadata=True
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0
    assert len(jmespath.search("related_occupations", result)) > 0

    first_hit = jmespath.search("related_occupations[0]", result)
    assert "metadata" in first_hit
    metadata_node = jmespath.search("metadata", first_hit)
    assert "enriched_ads_count" in metadata_node
    assert "enriched_ads_total_count" in metadata_node
    assert "enriched_ads_percent_of_total" in metadata_node
    assert "match_score" in metadata_node

    assert "enriched_candidates_term_frequency" not in metadata_node


@pytest.mark.parametrize(
    "input_text",
    [
        "Under utbildningen så lär du dig java, javascript, python, c# och testdriven utveckling.",
        "<div><p>Under utbildningen så lär du dig java, javascript, python, c# och testdriven utveckling.</p></div>",
    ],
)
# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_offset(occupations_matcher, input_text):
    result_no_offset = occupations_matcher.match_occupations_by_text(
        input_text, limit=10, include_metadata=False
    )
    result_offset = occupations_matcher.match_occupations_by_text(
        input_text, limit=10, offset=10, include_metadata=False
    )
    assert jmespath.search("hits_total", result_no_offset) > 0
    assert jmespath.search("hits_total", result_offset) > 0
    result_no_offset_hits = jmespath.search("related_occupations", result_no_offset)
    result_offset_hits = jmespath.search("related_occupations", result_offset)
    assert result_offset_hits[0] != result_no_offset_hits[0]


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_sjukskoterskeprogrammet_headline(
    occupations_matcher,
):
    # 'Sjuksköterskeprogrammet' should be rewritten to 'sjuksköterska' before enriching.
    input_headline = "Sjuksköterskeprogrammet"
    input_text = ""

    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0
    assert len(jmespath.search("related_occupations", result)) > 0

    assert "sjuksköterska" == jmespath.search(
        "identified_keywords_for_input.occupations | [0]", result
    )


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_zero_hits_occupation_in_headline(
    occupations_matcher,
):
    input_headline = "julsäljare"
    input_text = """arbetsmiljöarbete branschkunskap byggteknik byggvaruhandel cad cnc cnc-teknik engelska faner 
hantverksteknik ledarskap material möbler produktionsekonomi 
reglerteknik rådgivning serietillverkning sågverk trä träbranschen """

    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0
    assert len(jmespath.search("related_occupations", result)) > 0

    assert (
        len(jmespath.search("identified_keywords_for_input.occupations", result)) == 0
    )


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_competencies(occupations_matcher):
    input_headline = ""
    input_text = """felsökning java python"""

    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0
    assert len(jmespath.search("related_occupations", result)) > 0

    assert (
        len(jmespath.search("identified_keywords_for_input.occupations", result)) == 0
    )


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_by_text_jobtitle_with_zero_hits(occupations_matcher):
    input_headline = "julsäljare"
    input_text = " "

    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) == 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_with_few_parameters_should_return_zero_hits(
    occupations_matcher,
):
    input_headline = ""
    input_text = "java python"

    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) == 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_match_occupations_with_few_parameters_should_return_more_than_zero_hits(
    occupations_matcher,
):
    input_headline = ""
    input_text = "java python javascript"

    result = occupations_matcher.match_occupations_by_text(
        input_text, input_headline=input_headline, limit=10, include_metadata=False
    )
    # print(result)
    assert jmespath.search("hits_total", result) > 0


def get_test_education_by_id(id, dicts):
    return next((item for item in dicts if item["id"] == id), None)


def get_test_education_by_code(code, dicts):
    return next((item for item in dicts if item["education"]["code"] == code), None)


def load_ndjson_file(filepath):
    print("Loading json from file: %s" % filepath)
    with open(filepath, "r", encoding="utf-8") as file:
        data = ndjson.load(file)
        return data


if __name__ == "__main__":
    pytest.main([os.path.realpath(__file__), "-svv", "-ra"])
