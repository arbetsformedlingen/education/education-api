import json
import pytest
import requests
from requests.exceptions import HTTPError

test_url = "http://127.0.0.1:5000"
endpoint = "/v1/educations"

offset_limit = 9991


def _get(query: dict) -> dict:
    r = _get_raw(query)
    r.raise_for_status()
    response_json = json.loads(r.content)
    return response_json


def _get_raw(query: dict):
    r = requests.get(test_url + endpoint, params=query)
    return r


def test_char_limit_ok():
    query = f"{'x' * 998}"
    params = {"query": query}
    response = _get(params)


def test_char_limit_fail():
    with pytest.raises(HTTPError):
        query = f"{'x' * 999}"
        params = {"query": query}
        response = _get_raw(params)
        response_json = json.loads(response.content)
        assert response_json["custom"] == 'Wrong input'
        assert response.status_code == 400
        response.raise_for_status()


def test_offset_limit_ok():
    params = {"query": "", "offset": 9990}
    response = _get(params)


def test_offset_limit_fail():
    with pytest.raises(HTTPError):
        params = {"query": "", "offset": offset_limit}
        response = _get_raw(params)
        response_json = json.loads(response.content)
        assert response_json["custom"] == 'Wrong input'
        assert response.status_code == 400
        response.raise_for_status()
