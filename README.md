# JobEd Connect API

Endpoints for searching and matching related items in the labor market and the field of education.

## Install and run on localhost

Beware that a setup of OpenSearch is expected to run the project. The project expects the indices `educations-merged-enriched` and `enriched-occupations` to exist and contain proper data. These can be generated from scratch or populated from data dumps in Minio.

Python version >= 3.8 or higher should be used, probably using virtualenv or similar.

### Install dependency

The environment in use needs to contain education-opensearch.

#### Install education-opensearch from external repo

Clone repository

```shell
git clone git@gitlab.com:arbetsformedlingen/education/education-opensearch.git
```

Change directory to education-opensearch and install. Make sure to have virtual environment active.

```shell
pip install -r requirements.txt
python setup.py develop
```

Follow the [education-opensearch](https://gitlab.com/arbetsformedlingen/education/education-opensearch) readme for how to set up OpenSearch db on localhost or to connect to remote db.

### Install project dependencies

Make sure you are in the root of this repo and run:

```shell
pip install -r requirements.txt
```

## Running tests

Tests are run with pytest. Most of the tests are integration tests and as a consequence, a live OpenSearch server is needed.

Pay attention to environment variables, these ones are likely the ones:

| Variable          | Description |
|-------------------|-------------|
| `ES_HOST`         | OpenSearch host |
| `ES_PORT`         | OpenSearch port |
| `ES_USER`         | Username for OpenSearch |
| `ES_PWD`          | Password for OpenSearch |
| `ES_USE_SSL`      | Does OpenSearch run on https or http? |
| `ES_VERIFY_CERTS` | If OpenSearch uses TLS (https), should certs be verified? |

Check out the `educationapi/settings.py` file for all of the environment variables that can be used.

Running the tests from a terminal is as easy as running:

```shell
pytest
```

And if you need to specify some environment variables:

```shell
ES_HOST=127.0.0.1 ES_PORT=9200 ES_USER= ES_PWD= ES_USE_SSL=false pytest
```

## Running api in IntelliJ or PyCharm

- Activate a virtual env (e.g. conda or virtualenv):
  
- Using virtualenv: `File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment -(a Python 3.8 installation as base interpretator)`

- Run `educationapi/__init__.py`
- Example environment variables to provide when running against remote Open Search on AWS:

  ```shell
  ES_HOST=search-jobtech-opensearch-7zrtdhvq4feysy3peubhzsweai.eu-central-1.es.amazonaws.com;
  ES_USER=[ask for];
  ES_PORT=443;
  ES_PWD=[ask for]
  ```

- Example environment variables to provide when running against localhost Open Search:

  ```shell
  ES_USER=admin
  ES_PWD=admin
  ES_USE_SSL=False
  ```

Swagger is available on `http://localhost:5000/`.

## Podman

### Build

You might first need to login to jobtechdev docker mirror:
podman login docker-mirror.jobtechdev.se

Build container:

```shell
podman build -t educationapi:latest .
```

### Run

- Make sure there is a docker.env file in the root of repo (i.e. same level as the dockerfile). The docker.env  
  could contain the following env.variables:

```shell
JAE_API_URL=https://jobad-enrichments-test-api.jobtechdev.se
# Localhost
ES_HOST=host.containers.internal
ES_PORT=19200
ES_USER=
ES_PWD=
ES_USE_SSL=
ES_VERIFY_CERTS=
```

```shell
podman run -d --env-file=docker.env -p 5000:5000 --name educationapi educationapi
```

Swagger is available on `http://localhost:5000/`

#### Check log file

```shell
podman logs educationapi --details -f
```

#### Stop & remove image

```shell
podman stop educationapi;docker rm educationapi && true
```

Windows:

```shell
podman stop educationapi && docker rm educationapi || true
```

#### Debug Podman

```shell
podman logs educationapi --details -f
podman exec -t -i educationapi /bin/bash
podman run -it --rm educationapi:latest
```

#### Mac, remove previous podman-builds + cointainers

```shell
podman system prune
```
