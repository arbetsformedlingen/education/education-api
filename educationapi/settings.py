import os

ES_HOST = os.getenv("ES_HOST", "127.0.0.1").split(",")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER", "admin")
ES_PWD = os.getenv("ES_PWD", "admin")
ES_USE_SSL = os.getenv('ES_USE_SSL', 'true').lower() == 'true'
ES_VERIFY_CERTS = os.getenv('ES_VERIFY_CERTS', 'true').lower() == 'true'

ES_EDUCATIONS_ALIAS = os.getenv('ES_EDUCATIONS_ALIAS', 'educations-merged-enriched')

ES_ENRICHED_EDUCATIONS_ALIAS = os.getenv('ES_ENRICHED_EDUCATIONS_ALIAS', 'educations-merged-enriched')
ES_ENRICHED_OCCUPATIONS_ALIAS = os.getenv('ES_ENRICHED_OCCUPATIONS_ALIAS', 'enriched-occupations')

ENRICH_EDUCATIONS_OCCUPATIONS_THRESHOLD = os.getenv('ENRICH_EDUCATIONS_OCCUPATIONS_THRESHOLD', '0.7')
JAE_API_URL = os.getenv('JAE_API_URL', 'https://jobad-enrichments-test-api.jobtechdev.se')

TAXONOMY_API_HOST_URL = os.getenv('TAXONOMY_API_HOST_URL', 'https://taxonomy.api.jobtechdev.se')

JOBTECH_DATA_HOST_URL= os.getenv('JOBTECH_DATA_HOST_URL', 'https://data.jobtechdev.se')
JOBTECH_DATA_OCCUPATIONS_URL = f'{JOBTECH_DATA_HOST_URL}/taxonomy/version/21/query/occupations/occupations.json'

FILEPATH_AUTOMATIC_JOBTITLE_OCCUPATION_MAPPINGS = '../resources/jobtitle_occupation_tax_v21_mappings_thresh_0.5_ratio_500.csv'

ENRICHED_OCCUPATIONS_CACHE_EXPIRE_SECONDS = os.getenv('ENRICHED_OCCUPATIONS_CACHE_EXPIRE_SECONDS', 60 * 60 * 24)
TAXONOMY_OCCUPATIONS_CACHE_EXPIRE_SECONDS = os.getenv('ENRICHED_OCCUPATIONS_CACHE_EXPIRE_SECONDS', 60 * 60 * 24)

SEARCHPARAM_VALUES_CACHE_EXPIRE_SECONDS = os.getenv('SEARCHPARAM_VALUES_CACHE_EXPIRE_SECONDS', 60 * 60 * 1)

API_VERSION = '1.4.1'
API_VERSION_STATUS = ''
API_VERSION_RELEASED = '2024-05-07'

education_mappings = {
    "mappings": {
        "properties": {
            "id": {
                "type": "keyword",
            }
        }
    }
}
