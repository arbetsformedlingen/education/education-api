import logging

from flask import Flask
from flask_cors import CORS
from jobtech.common.customlogging import configure_logging

from educationapi.api import api

app = Flask(__name__, static_url_path='')
# Absolute path to root folder needed for local debugging, to avoid exception.
# app = Flask(__name__, instance_path="/Users/penms/Documents/git/education-api/instance")
CORS(app)

configure_logging([__name__.split('.')[0], 'educationapi'])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')
log.info(f"Starting: {__name__}")

def initialize_app(flask_app):
    api.init_app(flask_app)


def run_local_app():
    # Used only when starting this script directly, i.e. for debugging
    initialize_app(app)
    # app.run(debug=True)
    app.run(host='0.0.0.0', port=5000, debug=False)


if __name__ == '__main__':
    run_local_app()
else:
    # Main entrypoint
    initialize_app(app)
