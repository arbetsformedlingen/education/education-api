from educationapi.common.enriched_occupations_handler import EnrichedOccupationsHandler
from educationapi.common.model.search_educations_query_params import (
    SearchEducationsQueryParams,
)
from educationapi.common.opensearch_store import OpensearchStore
from educationapi.common.pagination_params import PaginationParams
from flask_restx import Resource, Api, reqparse, abort, inputs, Namespace
import time
import logging

from opensearchpy import RequestError

from educationapi import settings

from educationapi.common.opensearch_common import create_os_client
from educationapi.common.searchparam_values import SearchparamValues
from educationapi.matchers.occupations_matcher import OccupationsMatcher
from educationapi.matchers.educations_matcher import EducationsMatcher


version_number = f"{settings.API_VERSION} {settings.API_VERSION_STATUS}"

api = Api(
    title="JobEd Connect API",
    default="JobEd-Connect-API",
    version=version_number,
    description="Endpoints for searching and matching related items in the labor market "
    "and the field of education",
)

ns_liveness = Namespace("Namespace for liveness probe")
api.add_namespace(ns_liveness, "/")

ns_api_info = Namespace("Namespace for api info")
api.add_namespace(ns_api_info, "/")

ns_search_educations = Namespace(
    "Search educations", description="Search for educations"
)

api.add_namespace(ns_search_educations, "/")

ns_match_occupations = Namespace(
    "Match occupations",
    description="Match related occupations by for example an education or by competence terms in a text",
)
api.add_namespace(ns_match_occupations, "/")

ns_match_educations = Namespace(
    "Match educations",
    description="Match educations by for example an occupation or a job title",
)
api.add_namespace(ns_match_educations, "/")

ns_searchparam_values = Namespace(
    "Search parameter values",
    description="Search parameter values that should be used when filtering educations.",
)

api.add_namespace(ns_searchparam_values, "/")

log = logging.getLogger(__name__)

os_client = create_os_client()
enriched_occupations_handler = EnrichedOccupationsHandler(os_client)

opensearch_store = OpensearchStore(os_client)
occupations_matcher = OccupationsMatcher(os_client, enriched_occupations_handler)
educations_matcher = EducationsMatcher(os_client, enriched_occupations_handler)
searchparam_values = SearchparamValues(os_client)

endpoint_responses_type_1 = {200: "OK", 400: "Bad request", 500: "Technical error"}

endpoint_responses_type_2 = {
    200: "OK",
    400: "Bad request",
    404: "Not found",
    500: "Technical error",
}

HELP_EDUCATION_TYPE = "Filter on education type, e.g. program or kurs. See endpoint /v1/searchparameters/education_types for possible valid values."
HELP_EDUCATION_FORM = "Filter on education form, e.g. högskoleutbildning or yrkeshögskoleutbildning. See endpoint /v1/searchparameters/education_forms for possible valid values."
HELP_MUNICIPALITY_CODE = "Filter on 4 digit municipality code. See endpoint /v1/searchparameters/municipalities for possible valid values."
HELP_REGION_CODE = "Filter on 2 digit region (län) code. See endpoint /v1/searchparameters/regions for possible valid values."
HELP_DISTANCE = "Filter on educations that are at least partly from distance."
HELP_OFFSET_PARAM = "The offset parameter defines the index for the first result item you want to fetch."


@ns_liveness.route("liveness")
@ns_liveness.hide
class LivenessEndpoint(Resource):
    liveness_responses = {200: "OK", 500: "Technical error"}

    @ns_liveness.doc(
        responses=liveness_responses,
        description="Hidden endpoint for liveness probe in Openshift.",
    )
    def get(self):
        search_educations_query_params = SearchEducationsQueryParams()
        search_educations_query_params.free_text = "sjukvård"
        pagination_params = PaginationParams()
        pagination_params.limit = 1
        search_educations_result = None
        try:
            search_educations_result = opensearch_store.freetext_search(
                search_educations_query_params, pagination_params, True
            )
        except RequestError:
            abort(400, custom="Wrong input")
        has_search_educations_result = (
            search_educations_result["result"]
            and len(search_educations_result["result"]) > 0
        )

        match_occupations_result = occupations_matcher.match_occupations_by_text(
            "sjukvård", "sjuksköterska", limit=1, include_metadata=False
        )
        has_matched_occupations_result = (
            match_occupations_result and match_occupations_result["hits_returned"] > 0
        )

        # Note, Mattias P, 2023-01-19: Disabling logic for has_matched_educations_by_jobtitle_result since a shortage
        # of education data causes http 500/Not alive.
        # It's too risky to rely on the education data for /liveness at them moment.
        # matched_educations_by_jobtitle_result = educations_matcher.match_educations_by_jobtitle('sjuksköterska',
        #                                                                                         limit=1,
        #                                                                                         include_metadata=False)
        # has_matched_educations_by_jobtitle_result = matched_educations_by_jobtitle_result and \
        #                                              matched_educations_by_jobtitle_result['hits_total'] > 0

        # if has_search_educations_result and has_matched_occupations_result and has_matched_educations_by_jobtitle_result:
        if has_search_educations_result and has_matched_occupations_result:
            return "alive"
        else:
            abort(500, custom="Not alive")


## Endpoint that returns info about api
@ns_api_info.route("api_info")
@ns_api_info.hide
class ApiInfo(Resource):
    @api.doc()
    def get(self):
        return {
            "api_name": "JobEd Connect API",
            "api_version": settings.API_VERSION,
            "api_released": settings.API_VERSION_RELEASED,
            "api_documentation": "https://gitlab.com/arbetsformedlingen/education/education-api/-/blob/main/GETTING_STARTED.md",
            "api_status": settings.API_VERSION_STATUS,
        }


@ns_search_educations.param(
    "id", description="Unique Susa-Navet id for the education", example="i.sv.ft001"
)
@ns_search_educations.route("v1/educations/<id>")
class EducationV1(Resource):
    @api.doc(
        description="Endpoint for fetching education by id.",
        responses=endpoint_responses_type_2,
    )
    def get(self, id):
        if not id:
            abort(400, custom="missing id")
        result = opensearch_store.find_by_id(id)
        if not result:
            abort(404, custom="id not found")
        else:
            return result


@ns_search_educations.route("v1/educations")
class EducationListV1(Resource):
    EDUCATION_LIST_DEFAULT_LIMIT = 10
    education_list_parser = reqparse.RequestParser()
    education_list_parser.add_argument(
        "query",
        type=str,
        help="Free text search if provided. Otherwise retrieve all educations.",
    )
    education_list_parser.add_argument(
        "education_type", type=str, action="append", help=HELP_EDUCATION_TYPE
    )
    education_list_parser.add_argument(
        "education_form", type=str, action="append", help=HELP_EDUCATION_FORM
    )
    education_list_parser.add_argument(
        "municipality_code", type=str, help=HELP_MUNICIPALITY_CODE
    )
    education_list_parser.add_argument("region_code", type=str, help=HELP_REGION_CODE)
    education_list_parser.add_argument(
        "pace_of_study_percentage",
        type=str,
        help="Filter on pace of study percentage, e.g. 100",
    )
    education_list_parser.add_argument(
        "education_code", type=str, help="Filter on education_code"
    )
    education_list_parser.add_argument(
        "distance", type=inputs.boolean, default=False, help=HELP_DISTANCE
    )
    education_list_parser.add_argument(
        "filter_education_plan_exists",
        type=inputs.boolean,
        default=False,
        help="Filter on educations that has a scraped education plan. Intended for debug purpose.",
    )
    education_list_parser.add_argument(
        "limit",
        type=int,
        help="Number of results to fetch. Default value is %s."
        % EDUCATION_LIST_DEFAULT_LIMIT,
    )
    education_list_parser.add_argument("offset", type=int, help=HELP_OFFSET_PARAM)

    @api.doc(
        description="Endpoint for searching educations.",
        parser=education_list_parser,
        responses=endpoint_responses_type_1,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        args = self.education_list_parser.parse_args()

        search_educations_query_params = SearchEducationsQueryParams()
        pagination_params = PaginationParams()

        if args.get("query", None):
            search_educations_query_params.free_text = args["query"]
        if args.get("education_type"):
            search_educations_query_params.education_type = args["education_type"]
        if args.get("municipality_code"):
            search_educations_query_params.municipality_code = args["municipality_code"]
        if args.get("region_code"):
            search_educations_query_params.region_code = args["region_code"]
        if args.get("education_form"):
            search_educations_query_params.education_form = args["education_form"]
        if args.get("pace_of_study_percentage"):
            search_educations_query_params.pace_of_study_percentage = args[
                "pace_of_study_percentage"
            ]
        if args.get("education_code"):
            search_educations_query_params.education_code = args["education_code"]

        search_educations_query_params.distance = args.get("distance", False)
        search_educations_query_params.filter_education_plan_exists = args.get(
            "filter_education_plan_exists", False
        )

        if not args.get("limit"):
            pagination_params.limit = self.EDUCATION_LIST_DEFAULT_LIMIT
        if args.get("limit"):
            pagination_params.limit = args["limit"]
        if args.get("offset"):
            pagination_params.offset = args["offset"]

        full_search = True
        if search_educations_query_params:
            full_search = False

        # If search string (i.e. not full search):
        if full_search:
            result = self.find_by_full_search_pagination(pagination_params)
        # If empty search string (i.e. full search) and pagination:
        else:
            result = self.find_by_freetext(
                search_educations_query_params, pagination_params
            )

        log.debug(
            f"(v1 get educations) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result

    def find_by_full_search_pagination(self, pagination_params):
        # Note limitation that it's only possible to fetch first 10000 educations...
        try:
            return opensearch_store.full_search(pagination_params)
        except RequestError:
            abort(400, custom="Wrong input")

    def find_by_freetext(
        self,
        search_educations_query_params: SearchEducationsQueryParams,
        pagination_params: PaginationParams,
    ):
        # Note limitation that it's only possible to fetch first 10000 educations
        # (probably not a problem when proving search string to match)...
        try:
            return opensearch_store.freetext_search(
                search_educations_query_params, pagination_params, True
            )
        except RequestError:
            abort(400, custom="Wrong input")

    # def find_by_full_search(self):
    #     # TODO Inactivated full scan search because it usually ends with timeout or client out of memory...
    #     # result = opensearch_store.full_scan_search()
    #     result = opensearch_store.full_search(None, True)
    #     return Response(result, mimetype='application/json')


@ns_match_occupations.route("v1/occupations/match-by-education")
class MatchOccupationsByEducationIdV1(Resource):
    match_by_education_id_parser = reqparse.RequestParser()
    match_by_education_id_parser.add_argument(
        "education_id", required=True, type=str, help="Unique id for the education"
    )
    match_by_education_id_parser.add_argument(
        "limit",
        type=int,
        default=10,
        help="Number of results to fetch. Default value is 10.",
    )
    match_by_education_id_parser.add_argument(
        "offset", type=int, default=0, help=HELP_OFFSET_PARAM
    )
    match_by_education_id_parser.add_argument(
        "include_metadata",
        type=inputs.boolean,
        default=False,
        help="Return additional metadata. Defalt value is False.",
    )

    @api.doc(
        description="Endpoint for matching occupations for an education id.",
        parser=match_by_education_id_parser,
        responses=endpoint_responses_type_2,
    )
    def post(self):
        start_time = int(time.time() * 1000)
        args = self.match_by_education_id_parser.parse_args()
        input_education_id = args.get("education_id", None)
        if not input_education_id:
            abort(400, custom="missing education_id")
        input_limit = args.get("limit", 10)
        input_offset = args.get("offset", 0)
        input_include_metadata = args.get("include_metadata", False)

        result = occupations_matcher.match_occupations_by_id(
            input_education_id,
            limit=input_limit,
            offset=input_offset,
            include_metadata=input_include_metadata,
        )
        if not result:
            abort(404, custom="education_id not found")
        else:
            log.debug(
                f"(v1/match-by-education) Took: {int(time.time() * 1000 - start_time)} milliseconds"
            )
            return result


@ns_match_occupations.route("v1/occupations/match-by-education-code")
class MatchOccupationsByEducationCodeV1(Resource):
    match_by_education_code_parser = reqparse.RequestParser()
    match_by_education_code_parser.add_argument(
        "education_code", required=True, type=str, help="Unique code for the education"
    )
    match_by_education_code_parser.add_argument(
        "limit",
        type=int,
        default=10,
        help="Number of results to fetch. Default value is 10.",
    )
    match_by_education_code_parser.add_argument(
        "offset", type=int, default=0, help=HELP_OFFSET_PARAM
    )
    match_by_education_code_parser.add_argument(
        "include_metadata",
        type=inputs.boolean,
        default=False,
        help="Return additional metadata. Defalt value is False.",
    )

    @api.doc(
        description="Endpoint for matching occupations for an education id.",
        parser=match_by_education_code_parser,
        responses=endpoint_responses_type_2,
    )
    def post(self):
        start_time = int(time.time() * 1000)
        args = self.match_by_education_code_parser.parse_args()
        input_education_code = args.get("education_code", None)
        if not input_education_code:
            abort(400, custom="missing education_code")
        input_limit = args.get("limit", 10)
        input_offset = args.get("offset", 0)
        input_include_metadata = args.get("include_metadata", False)

        result = occupations_matcher.match_occupations_by_education_code(
            input_education_code,
            limit=input_limit,
            offset=input_offset,
            include_metadata=input_include_metadata,
        )
        if not result:
            abort(404, custom="education_code not found")
        else:
            log.debug(
                f"(v1/match-by-education-code) Took: {int(time.time() * 1000 - start_time)} milliseconds"
            )
            return result


@ns_match_occupations.route("v1/occupations/match-by-text")
class MatchOccupationsByTextV1(Resource):
    match_by_education_id_parser = reqparse.RequestParser()
    match_by_education_id_parser.add_argument(
        "input_text",
        required=True,
        type=str,
        help="Text that contains for example competencies that one learns when studying.",
    )
    match_by_education_id_parser.add_argument(
        "input_headline",
        required=False,
        type=str,
        help="Text that contains the headline for the education.",
    )
    match_by_education_id_parser.add_argument(
        "limit",
        type=int,
        default=10,
        help="Number of results to fetch. Default value is 10.",
    )
    match_by_education_id_parser.add_argument(
        "offset", type=int, default=0, help=HELP_OFFSET_PARAM
    )
    match_by_education_id_parser.add_argument(
        "include_metadata",
        type=inputs.boolean,
        default=False,
        help="Return additional metadata. Defalt value is False.",
    )

    @api.doc(
        description="Endpoint for matching occupations from a text that contains for example competencies or job titles.",
        parser=match_by_education_id_parser,
        responses=endpoint_responses_type_1,
    )
    def post(self):
        start_time = int(time.time() * 1000)
        args = self.match_by_education_id_parser.parse_args()
        input_text = args.get("input_text", None)
        if not input_text or not input_text.strip():
            abort(400, custom="missing input_text")
        input_headline = args.get("input_headline", None)
        input_limit = args.get("limit", 10)
        input_offset = args.get("offset", 0)
        input_include_metadata = args.get("include_metadata", False)

        result = occupations_matcher.match_occupations_by_text(
            input_text,
            input_headline,
            limit=input_limit,
            offset=input_offset,
            include_metadata=input_include_metadata,
        )

        log.debug(
            f"(v1/match-by-text) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result


@ns_match_occupations.route("v1/enriched_occupations")
class EnrichedOccupationsDetailsV1(Resource):
    enriched_occupations_parser = reqparse.RequestParser()
    enriched_occupations_parser.add_argument(
        "occupation_id",
        required=True,
        type=str,
        help="Unique id for the occupation according to Taxonomy concept_id",
    )
    enriched_occupations_parser.add_argument(
        "include_metadata",
        type=inputs.boolean,
        default=False,
        help="Return additional metadata. Defalt value is False.",
    )

    @api.doc(
        description="Endpoint for getting details for an enriched occupation.",
        parser=enriched_occupations_parser,
        responses=endpoint_responses_type_2,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        args = self.enriched_occupations_parser.parse_args()
        occupation_id = args.get("occupation_id", None)
        if not occupation_id:
            abort(400, custom="missing occupation_id")
        input_include_metadata = args.get("include_metadata", False)

        result = occupations_matcher.get_occupation_details(
            occupation_id, include_metadata=input_include_metadata
        )
        if not result:
            abort(404, custom="no result could be found")
        else:
            log.debug(
                f"(v1/enriched_occupations) Took: {int(time.time() * 1000 - start_time)} milliseconds"
            )
            return result


@ns_match_educations.route("v1/educations/match-by-occupation")
class MatchEducationsByOccupationIdV1(Resource):
    match_by_occupation_id_parser = reqparse.RequestParser()
    match_by_occupation_id_parser.add_argument(
        "occupation_id", required=True, type=str, help="Unique id for the occupation"
    )
    match_by_occupation_id_parser.add_argument(
        "education_type", type=str, action="append", help=HELP_EDUCATION_TYPE
    )
    match_by_occupation_id_parser.add_argument(
        "education_form", type=str, action="append", help=HELP_EDUCATION_FORM
    )
    match_by_occupation_id_parser.add_argument(
        "municipality_code", type=str, help=HELP_MUNICIPALITY_CODE
    )
    match_by_occupation_id_parser.add_argument(
        "region_code", type=str, help=HELP_REGION_CODE
    )
    match_by_occupation_id_parser.add_argument(
        "distance", type=inputs.boolean, default=False, help=HELP_DISTANCE
    )
    match_by_occupation_id_parser.add_argument(
        "limit",
        type=int,
        default=10,
        help="Number of results to fetch. Default value is 10.",
    )
    match_by_occupation_id_parser.add_argument(
        "offset", type=int, default=0, help=HELP_OFFSET_PARAM
    )
    match_by_occupation_id_parser.add_argument(
        "include_metadata",
        type=inputs.boolean,
        default=False,
        help="Return additional metadata. Defalt value is False.",
    )

    @api.doc(
        description="Endpoint for matching educations for a taxonomy occupation conceptId.",
        parser=match_by_occupation_id_parser,
        responses=endpoint_responses_type_2,
    )
    def post(self):
        start_time = int(time.time() * 1000)
        args = self.match_by_occupation_id_parser.parse_args()
        input_occupation_id = args.get("occupation_id", None)
        if not input_occupation_id:
            abort(400, custom="missing occupation_id")

        input_education_type = args.get("education_type", None)
        input_education_form = args.get("education_form", None)
        input_municipality_code = args.get("municipality_code", None)
        input_region_code = args.get("region_code", None)
        input_distance = args.get("distance", False)
        input_limit = args.get("limit", 10)
        input_offset = args.get("offset", 0)
        input_include_metadata = args.get("include_metadata", False)

        result = educations_matcher.match_educations_by_occupation_id(
            input_occupation_id,
            education_type=input_education_type,
            education_form=input_education_form,
            municipality_code=input_municipality_code,
            region_code=input_region_code,
            distance=input_distance,
            limit=input_limit,
            offset=input_offset,
            include_metadata=input_include_metadata,
        )
        if not result:
            abort(404, custom="occupation_id not found")
        else:
            log.debug(
                f"(v1/match-by-occupation) Took: {int(time.time() * 1000 - start_time)} milliseconds"
            )
            return result


@ns_match_educations.route("v1/educations/match-by-jobtitle")
class MatchEducationsByJobtitleV1(Resource):
    match_by_jobtitle_parser = reqparse.RequestParser()
    match_by_jobtitle_parser.add_argument(
        "jobtitle",
        required=True,
        type=str,
        help="Jobtitle according to the endpoint /v1/jobtitles/autocomplete",
    )
    match_by_jobtitle_parser.add_argument(
        "education_type", type=str, action="append", help=HELP_EDUCATION_TYPE
    )
    match_by_jobtitle_parser.add_argument(
        "education_form", type=str, action="append", help=HELP_EDUCATION_FORM
    )
    match_by_jobtitle_parser.add_argument(
        "municipality_code", type=str, help=HELP_MUNICIPALITY_CODE
    )
    match_by_jobtitle_parser.add_argument(
        "region_code", type=str, help=HELP_REGION_CODE
    )
    match_by_jobtitle_parser.add_argument(
        "distance", type=inputs.boolean, default=False, help=HELP_DISTANCE
    )
    match_by_jobtitle_parser.add_argument(
        "limit",
        type=int,
        default=10,
        help="Number of results to fetch. Default value is 10.",
    )
    match_by_jobtitle_parser.add_argument(
        "offset", type=int, default=0, help=HELP_OFFSET_PARAM
    )
    match_by_jobtitle_parser.add_argument(
        "include_metadata",
        type=inputs.boolean,
        default=False,
        help="Return additional metadata. Defalt value is False.",
    )

    @api.doc(
        description="Endpoint for matching educations for a taxonomy occupation conceptId.",
        parser=match_by_jobtitle_parser,
        responses=endpoint_responses_type_2,
    )
    def post(self):
        start_time = int(time.time() * 1000)
        args = self.match_by_jobtitle_parser.parse_args()
        input_jobtitle = args.get("jobtitle", None)
        if not input_jobtitle:
            abort(400, custom="missing jobtitle")
        input_education_type = args.get("education_type", None)
        input_education_form = args.get("education_form", None)
        input_municipality_code = args.get("municipality_code", None)
        input_region_code = args.get("region_code", None)
        input_distance = args.get("distance", False)
        input_limit = args.get("limit", 10)
        input_offset = args.get("offset", 0)
        input_include_metadata = args.get("include_metadata", False)

        result = educations_matcher.match_educations_by_jobtitle(
            input_jobtitle,
            education_type=input_education_type,
            education_form=input_education_form,
            municipality_code=input_municipality_code,
            region_code=input_region_code,
            distance=input_distance,
            limit=input_limit,
            offset=input_offset,
            include_metadata=input_include_metadata,
        )
        if not result:
            abort(404, custom="jobtitle not found")
        else:
            log.debug(
                f"(v1/match-by-jobtitle) Took: {int(time.time() * 1000 - start_time)} milliseconds"
            )
            return result


@ns_match_educations.route("v1/jobtitles/autocomplete")
class JobtitlesAutocompleteV1(Resource):
    jobtitles_autocomplete_parser = reqparse.RequestParser()
    jobtitles_autocomplete_parser.add_argument(
        "word",
        required=True,
        type=str,
        help="Word or part of word to get autocompleted jobtitles for",
    )
    jobtitles_autocomplete_parser.add_argument(
        "limit",
        type=int,
        default=10,
        help="Number of results to fetch. Default value is 10.",
    )

    @api.doc(
        description="Endpoint for matching educations for a taxonomy occupation conceptId.",
        parser=jobtitles_autocomplete_parser,
        responses=endpoint_responses_type_1,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        args = self.jobtitles_autocomplete_parser.parse_args()
        input_word = args.get("word", None)
        if not input_word:
            abort(400, custom="missing input word")
        input_limit = args.get("limit", 10)

        result = educations_matcher.autocomplete_jobtitles(input_word, size=input_limit)

        log.debug(
            f"(v1/jobtitles/autocomplete) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result


@ns_searchparam_values.route("v1/searchparameters/education_forms")
class SearchParametersEducationFormsV1(Resource):
    @api.doc(
        description="Endpoint for getting valid education forms for filtering educations.",
        responses=endpoint_responses_type_1,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        result = searchparam_values.get_education_forms()

        log.debug(
            f"(v1/searchparameters/education_forms) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result


@ns_searchparam_values.route("v1/searchparameters/education_types")
class SearchParametersEducationTypesV1(Resource):
    @api.doc(
        description="Endpoint for getting valid education types for filtering educations.",
        responses=endpoint_responses_type_1,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        result = searchparam_values.get_education_types()

        log.debug(
            f"(v1/searchparameters/education_types) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result


@ns_searchparam_values.route("v1/searchparameters/municipalities")
class SearchParametersMunicipalitiesV1(Resource):
    @api.doc(
        description="Endpoint for getting valid municipalities for filtering educations.",
        responses=endpoint_responses_type_1,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        result = searchparam_values.get_municipality_codes()

        log.debug(
            f"(v1/searchparameters/municipalities) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result


@ns_searchparam_values.route("v1/searchparameters/regions")
class SearchParametersRegionsV1(Resource):
    @api.doc(
        description="Endpoint for getting valid regions (län) for filtering educations.",
        responses=endpoint_responses_type_1,
    )
    def get(self):
        start_time = int(time.time() * 1000)
        result = searchparam_values.get_region_codes()

        log.debug(
            f"(v1/searchparameters/regions) Took: {int(time.time() * 1000 - start_time)} milliseconds"
        )
        return result
