from datetime import datetime
import json
import logging
import operator
import os
from collections import Counter
from collections import OrderedDict

import jmespath
import pandas as pd
import requests
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from fast_autocomplete import AutoComplete

from educationapi import settings
from educationapi.common.helper_functions import *
from educationapi.common.synonym_dictionary import SynonymDictionary

currentdir = os.path.dirname(os.path.realpath(__file__)) + "/"

# File with jobtitles that will be blacklisted in autocomplete endpoints
FILEPATH_EDUCATION_MATCHER_BLACKLIST = (
    f"{currentdir}../resources/blacklist_educations_matcher_jobtitles.txt"
)

# File with jobtitles that will be blacklisted in education matcher endpoints
FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST = (
    f"{currentdir}../resources/blacklist_educations_matcher_occupations.csv"
)

# File with automatically created mappings between a jobtitle and an occupation. Used when matching educations from a jobtitle.
FILEPATH_AUTOMATIC_JOBTITLE_OCCUPATION_MAPPINGS = (
    currentdir + settings.FILEPATH_AUTOMATIC_JOBTITLE_OCCUPATION_MAPPINGS
)

# File with manual mappings between a jobtitle and an occupation. Used when matching educations from a jobtitle.
FILEPATH_MANUAL_JOBTITLE_OCCUPATION_MAPPINGS = (
    currentdir + "../resources/jobtitle_occupation_mappings_manual.csv"
)

# Maximum number of allowed parameters in Opensearch
OPENSEARCH_MAX_CLAUSE_COUNT = 1024
MAX_COMPETENCIES_INPUT_COUNT = 1024
MAX_OCCUPATIONS_INPUT_COUNT = 2

MIN_COMP_SHOULD_MATCH = "2<75% 4<50% 10<10% 20<6% 50<5% 80<4% 100<3% 200<2%"


class EducationsMatcher(object):
    cache_opts = {
        "cache.type": "memory",
        "cache.expire": settings.TAXONOMY_OCCUPATIONS_CACHE_EXPIRE_SECONDS,
    }

    cache = CacheManager(**parse_cache_config_options(cache_opts))

    DEFAULT_RESULT_LIMIT = 10
    DEFAULT_RESULT_OFFSET = 0

    def __init__(self, os_client, enriched_occupations_handler):
        self.log = logging.getLogger(__name__)
        self.log.info("Initializing EducationsMatcher")
        self.synonym_dictionary = None
        self.jobtitle_occupation_mappings = None
        self.all_enriched_occupations = None

        self.autocomplete = None

        self.os_client = os_client
        self.enriched_occupations_handler = enriched_occupations_handler

    def _init_autocomplete(self):
        autocomplete_valid_chars = "abcdefghijklmnopqrstuvwxyzåäö"
        autocomplete_valid_chars += autocomplete_valid_chars.upper()
        autocomplete_valid_chars += "#-+."

        self.autocomplete = AutoComplete(
            words=self._get_all_valid_autocomplete_terms_and_occupation_ids(),
            valid_chars_for_string=autocomplete_valid_chars,
        )

    def match_educations_by_occupation_id(
        self,
        occupation_taxonomy_concept_id,
        education_type=None,
        education_form=None,
        municipality_code=None,
        region_code=None,
        distance=False,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
        include_metadata=False,
    ):
        """
        Matches most relevant educations for an occupation.
        :param occupation_taxonomy_concept_id: unique taxonomy conceptId
        :param education_type: education type, e.g. program or kurs.
        :param education_form: education form, e.g. högskoleutbildning or yrkeshögskoleutbildning
        :param municipality_code: 4 digit municipality code.
        :param region_code: 2 digit region (län) code.
        :param distance: If the flag eventSummary.distance should be True
        :param limit: Maximum number of hits
        :param offset: Start result from index
        :param include_metadata: True if metadata should be included
        """
        if not occupation_taxonomy_concept_id:
            return None

        limit, offset = self._adjust_limit_and_offset(limit, offset)

        educations_alias_name = settings.ES_ENRICHED_EDUCATIONS_ALIAS

        educations_source_excludes_fields = ["events.*"]

        # Get the enriched occupation so the matching parameters can be created from the enriched values.
        occupation_details = (
            self.enriched_occupations_handler.find_occupation_by_concept_id(
                occupation_taxonomy_concept_id
            )
        )

        if not occupation_details:
            self.log.info(
                "No occupation was found: %s" % occupation_taxonomy_concept_id
            )
            return None

        """
        Filter out occupations that lead to unexpected search
        results for education
        """
        df_blacklist = self._load_occupations_blacklist_items()

        if (
            df_blacklist["occupation_concept_id"]
            .str.contains(occupation_taxonomy_concept_id)
            .any()
        ):
            self.log.info(
                "Occupation is blacklisted: %s" % occupation_taxonomy_concept_id
            )
            return None

        if occupation_details:
            occupation_params = self.create_occupation_params(occupation_details)

            self.limit_params_for_max_clause_count(
                occupation_params,
                education_type,
                education_form,
                municipality_code,
                region_code,
                distance,
            )

            query = self.create_match_educations_query(
                occupation_params["limited_competencies_term_boost"],
                occupation_params["limited_occupations_term_boost"],
                education_type=education_type,
                education_form=education_form,
                municipality_code=municipality_code,
                region_code=region_code,
                distance=distance,
            )

            self.log.debug(json.dumps(query, indent=4))

            education_match_results = self.os_client.search(
                index=educations_alias_name,
                body=query,
                search_type="dfs_query_then_fetch",
                size=limit,
                from_=offset,
                track_total_hits=True,
                _source_excludes=educations_source_excludes_fields,
            )

            formatted_education_match_results = self.format_education_match_results(
                education_match_results, include_metadata
            )
            if include_metadata:
                limited_competencies_term_boost = jmespath.search(
                    "limited_competencies_term_boost", occupation_params
                )
                limited_occupations_term_boost = jmespath.search(
                    "limited_occupations_term_boost", occupation_params
                )
                metadata = {
                    "occupation_id": jmespath.search("concept_id", occupation_details),
                    "occupation_label": jmespath.search("label", occupation_details),
                    "occupation_enriched_ads_count": jmespath.search(
                        "enriched_ads_count", occupation_details
                    ),
                    "competencies_term_boost": limited_competencies_term_boost,
                    "competencies_size": len(limited_competencies_term_boost),
                    "occupations_term_boost": limited_occupations_term_boost,
                    "occupations_size": len(limited_occupations_term_boost),
                }
                formatted_education_match_results["metadata"] = metadata

            return formatted_education_match_results

        return None

    # FIXME: This method should be called from create_match_educations_query instead to make it easier to calculate the number of parameters (and start params_count from 0).
    def limit_params_for_max_clause_count(
        self,
        occupation_params,
        education_type,
        education_form,
        municipality_code,
        region_code,
        distance,
    ):
        """
        Ensures that the number of parameters won't exceed the OPENSEARCH_MAX_CLAUSE_COUNT.
        Without this limit, Opensearch will throw an exception because of too many clauses (1024) in the query.
        """
        params_count = 7 # Start at a positive number since datefilter is always included
        if education_type:
            params_count += len(education_type)
        if education_form:
            params_count += len(education_form)
        if municipality_code:
            params_count += 1
        if region_code:
            params_count += 1
        if distance:
            params_count += 1
        if occupation_params["limited_competencies_term_boost"]:
            params_count += len(occupation_params["limited_competencies_term_boost"])
        if occupation_params["limited_occupations_term_boost"]:
            params_count += len(occupation_params["limited_occupations_term_boost"])
        self.log.debug(f"params_count {params_count} for query")
        if params_count > OPENSEARCH_MAX_CLAUSE_COUNT:
            exceed_count = params_count - OPENSEARCH_MAX_CLAUSE_COUNT
            for i in range(0, exceed_count):
                removed_item = occupation_params[
                    "limited_competencies_term_boost"
                ].popitem(last=True)
                self.log.debug(f"Removed item {removed_item}")

    def _adjust_limit_and_offset(self, limit, offset):
        if offset < 0:
            offset = self.DEFAULT_RESULT_OFFSET
        if limit <= 0:
            limit = self.DEFAULT_RESULT_LIMIT
        return limit, offset

    def match_educations_by_jobtitle(
        self,
        jobtitle,
        education_type=None,
        education_form=None,
        municipality_code=None,
        region_code=None,
        distance=False,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
        include_metadata=False,
    ):
        """
        Matches most relevant educations for a jobtitle. The jobtitle will be mapped to the most likely occupation.
        :param jobtitle: For example "sjuksköterska"
        :param education_type: education type, e.g. program or kurs.
        :param education_form: education form, e.g. högskoleutbildning or yrkeshögskoleutbildning
        :param municipality_code: 4 digit municipality code.
        :param region_code: 2 digit region (län) code.
        :param distance: If the flag eventSummary.distance should be True
        :param limit: Maximum number of hits
        :param offset: Start result from index
        :param include_metadata: True if metadata should be included
        """
        match_result = None
        mapped_occupation = self._get_mapped_occupation_for_jobtitle(jobtitle)
        mapped_occupation_id = jmespath.search(
            "occupation_concept_id", mapped_occupation
        )
        mapped_occupation_label = jmespath.search("occupation_label", mapped_occupation)
        self.log.debug(
            "Input/jobtitle: %s - Found mapped occupation: %s (%s)"
            % (jobtitle, mapped_occupation_label, mapped_occupation_id)
        )

        if mapped_occupation_id:
            match_result = self.match_educations_by_occupation_id(
                mapped_occupation_id,
                education_type=education_type,
                education_form=education_form,
                municipality_code=municipality_code,
                region_code=region_code,
                distance=distance,
                limit=limit,
                offset=offset,
                include_metadata=include_metadata,
            )
            # Return the occupation that was mapped for the jobtitle.
            mapped_occupation_dict = {"mapped_occupation_for_match": mapped_occupation}
            # Put the attribute 'mapped_occupation_for_match' first in the match_result dict.
            if match_result:
                mapped_occupation_dict.update(match_result)
            else:
                mapped_occupation_dict["hits_total"] = 0
                mapped_occupation_dict["hits"] = []
            match_result = mapped_occupation_dict
        return match_result

    def autocomplete_jobtitles(self, word, size=10):
        """
        Autocomplete function for jobtitles
        :param word: Input, for example "sjuksk" that will return "sjuksköterska"
        :param size: How many results there will be in the response.
        :return: A list with objects that contains jobtitle and corresponding occupation_label
        """
        """
        applying lowercasing to make the search case-insensitive.
        """
        if not self.autocomplete:
            self._init_autocomplete()

        word = word.lower()
        autocomplete_result = self.autocomplete.search(word=word, max_cost=3, size=size)
        extended_autocomplete_result = []
        valid_term_startswith = []
        valid_term_startswith.append(word)
        valid_term_startswith.append(word.replace(" ", ""))
        valid_term_startswith.append(word.replace(" ", "-"))
        valid_term_startswith.append(word.replace("-", " "))
        valid_term_startswith = list(set(valid_term_startswith))

        """
        Filter out occupations that lead to unexpected search results for
        education
        """
        autocomplete_blacklist = self._load_jobtitle_blacklist_items()
        autocomplete_blacklist = [[i] for i in autocomplete_blacklist]
        autocomplete_result = [
            i for i in autocomplete_result if i not in autocomplete_blacklist
        ]

        for result_terms in autocomplete_result:
            for term in result_terms:
                if any(
                    term.startswith(valid_word) for valid_word in valid_term_startswith
                ):
                    extended_term = self._create_extended_term(term)
                    if extended_term not in extended_autocomplete_result:
                        extended_autocomplete_result.append(extended_term)
        return extended_autocomplete_result

    def _load_jobtitle_blacklist_items(self):
        if not os.path.isfile(FILEPATH_EDUCATION_MATCHER_BLACKLIST):
            return []

        with open(
            FILEPATH_EDUCATION_MATCHER_BLACKLIST, encoding="utf-8"
        ) as stoplistfile:
            blacklist_items = stoplistfile.readlines()
        blacklist_items = [x.strip() for x in blacklist_items]
        return blacklist_items

    def _load_occupations_blacklist_items(self):
        if not os.path.isfile(FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST):
            return []

        blacklist_items = pd.read_csv(
            FILEPATH_EDUCATION_MATCHER_OCCUPATIONS_BLACKLIST,
            usecols=["occupation_label", "occupation_concept_id"],
            sep=";",
            keep_default_na=False,
        )

        return blacklist_items

    def _create_extended_term(self, term):
        if not self.autocomplete:
            self._init_autocomplete()
        autocomplete_obj = self.autocomplete.words[term]
        occupation_id = jmespath.search("occupation_concept_id", autocomplete_obj)
        occupation_label = jmespath.search("occupation_label", autocomplete_obj)
        extended_term = {
            "jobtitle": term,
            "occupation_id": occupation_id,
            "occupation_label": occupation_label,
            "occupation_group": jmespath.search("occupation_group", autocomplete_obj),
        }
        return extended_term

    def format_education_match_results(
        self, education_match_results, include_metadata=False
    ):
        """
        Formats the results with the wanted structure for output in the response.
        """
        formatted_hits = []

        hits_total = jmespath.search("hits.total.value", education_match_results)
        hits = jmespath.search("hits.hits", education_match_results)

        for hit in hits:
            match_score = jmespath.search("_score", hit)
            source_item = jmespath.search("_source", hit)
            education = jmespath.search("education", source_item)
            education_title = jmespath.search(
                "title[?lang=='swe'].content | [0]", education
            )
            if not education_title:
                education_title = ""
            education_description = jmespath.search(
                "description[?lang=='swe'].content | [0]", education
            )
            if not education_description:
                education_description = ""

            education_provider_name = jmespath.search(
                "education_providers[0].name[?lang=='swe'].content | [0]", source_item
            )
            if not education_provider_name:
                education_provider_name = ""

            education_result_object = {
                "id": jmespath.search("identifier", education),
                "code": jmespath.search("code", education),
                "education_provider_name": education_provider_name,
                "expires": jmespath.search("expires", education),
                "education_title": education_title,
                "education_description": education_description,
                "education_type": jmespath.search("configuration.code", education),
                "education_form": jmespath.search("form.code", education),
                "providerSummary": jmespath.search("providerSummary", source_item),
                "eventSummary": jmespath.search("eventSummary", source_item),
            }

            if include_metadata:
                education_result_object["metadata"] = {"match_score": match_score}

            formatted_hits.append(education_result_object)

        formatted_results = {"hits_total": hits_total, "hits": formatted_hits}

        return formatted_results

    def create_match_educations_query(
        self,
        competencies_term_boost={},
        occupations_term_boost={},
        education_type=None,
        education_form=None,
        municipality_code=None,
        region_code=None,
        distance=False,
    ):
        should_node = []

        for competence_term, competence_boost in competencies_term_boost.items():
            competence_term = format_input_term(competence_term)
            termquery = {
                "term": {
                    "text_enrichments_results.enriched_candidates_complete.competencies": {
                        "value": competence_term,
                        "boost": competence_boost,
                    }
                }
            }
            should_node.append(termquery)

        for occupation_term, occupation_boost in occupations_term_boost.items():
            occupation_term = format_input_term(occupation_term)
            termquery = {
                "term": {
                    "text_enrichments_results.enriched_candidates_complete.occupations": {
                        "value": occupation_term,
                        "boost": occupation_boost,
                    }
                }
            }
            should_node.append(termquery)

        must_list = []

        # Education type filter (e.g. 'program' or 'kurs'):
        if education_type:
            education_type_query = self._create_multiple_terms_query(
                education_type, "education.configuration.code"
            )
            must_list.append(education_type_query)
        # Ecucation form filter (e.g. 'högskoleutbildning' or 'yrkeshögskoleutbildning':
        if education_form:
            education_type_query = self._create_multiple_terms_query(
                education_form, "education.form.code"
            )
            must_list.append(education_type_query)
        if municipality_code:
            must_list.append(
                {"term": {"events.locations.municipalityCode": municipality_code}}
            )
        if region_code:
            must_list.append({"term": {"events.locations.regionCode": region_code}})
        if distance:
            must_list.append({"term": {"eventSummary.distance": True}})

        # Date filter for executions to ensure expired educations are not displayed
        # - If 'start' date exists, it must be greater than today.
        # - If 'start' is missing, 'end' date must be greater than today.
        # - If both 'start' and 'end' are missing, the education will still be displayed.

        current_date = datetime.now().strftime('%Y-%m-%d')

        date_filter_query = {
            "bool": {
                "should": [
                    {
                        "bool": {
                            "must": [
                                {"range": {"eventSummary.executions.start": {"gt": current_date}}}
                            ]
                        }
                    },
                    {
                        "bool": {
                            "must_not": [
                                {"exists": {"field": "eventSummary.executions.start"}}
                            ],
                            "must": [
                                {"range": {"eventSummary.executions.end": {"gt": current_date}}}
                            ]
                        }
                    },
                    {
                        "bool": {
                            "must_not": [
                                {"exists": {"field": "eventSummary.executions.start"}},
                                {"exists": {"field": "eventSummary.executions.end"}}
                            ]
                        }
                    }
                ],
                "minimum_should_match": 1
            }
        }

        must_list.append(date_filter_query)

        query = {
            "query": {
                "bool": {
                    "should": should_node,
                    "must": must_list,
                    "minimum_should_match": MIN_COMP_SHOULD_MATCH,
                }
            }
        }

        self.log.debug(json.dumps(query, ensure_ascii=False))
        return query

    def _create_multiple_terms_query(self, query_term_list, search_field_name):
        boolean_node = {"bool": {"should": []}}
        boolean_should_node = boolean_node["bool"]["should"]
        for query_term in query_term_list:
            boolean_should_node.append(
                {"term": {search_field_name: query_term.lower()}}
            )
        return boolean_node

    def create_occupation_params(self, occupation_details):
        """
        Creates values that will be used in the query. Also returns values that will be returned in the
        complete response.
        :param occupation_details: An enriched occupation with competencies and jobtitles.
        """
        occupation_hit = occupation_details
        occupation_id = jmespath.search("concept_id", occupation_hit)
        occupation_label_result = jmespath.search("label", occupation_hit)

        self.log.debug(
            "Occupation: %s (id: %s)" % (occupation_label_result, occupation_id)
        )
        occupation_group_label = jmespath.search(
            "occupation_group.label", occupation_hit
        )

        self.log.debug("Occupation group: %s" % occupation_group_label)
        enriched_ads_count = jmespath.search("enriched_ads_count", occupation_hit)
        if enriched_ads_count:
            self.log.debug(
                "Number of enriched ads for occupation: %s" % (enriched_ads_count)
            )

        competencies = jmespath.search(
            "enriched_candidates.competencies", occupation_hit
        )

        competencies_term_boost = self.create_boost_values(competencies)
        limited_competencies_term_boost = self.limit_terms_boost(
            competencies_term_boost, MAX_COMPETENCIES_INPUT_COUNT
        )
        # Sort by value desc and key for a nicer presentation.
        self.log.debug(
            "limited_competencies_term_boost (%s): %s"
            % (len(limited_competencies_term_boost), limited_competencies_term_boost)
        )
        occupations = jmespath.search("enriched_candidates.occupations", occupation_hit)

        occupations_term_boost = self.create_boost_values(
            occupations, multiply_value=1.5
        )
        limited_occupations_term_boost = self.limit_terms_boost(
            occupations_term_boost, MAX_OCCUPATIONS_INPUT_COUNT
        )
        self.log.debug(
            "limited_occupations_term_boost (%s): %s"
            % (len(limited_occupations_term_boost), limited_occupations_term_boost)
        )
        occupation_params = {
            "occupation_id": occupation_id,
            "occupation_label_result": occupation_label_result,
            "occupation_group_label": occupation_group_label,
            "enriched_ads_count": enriched_ads_count,
            "competencies": competencies,
            "limited_competencies_term_boost": limited_competencies_term_boost,
            "occupations": occupations,
            "limited_occupations_term_boost": limited_occupations_term_boost,
        }
        return occupation_params

    def create_boost_values(self, enriched_terms, multiply_value=1.0):
        """
        Creates a dict with terms and a boost value for each term
        :param enriched_terms: list with terms to create a boost value for
        :param multiply_value: How many times the boost value should be multiplied
        """
        if not enriched_terms:
            return {}

        terms_tf_idf = self.enriched_occupations_handler.get_terms_tf_idf()

        terms_count = len(enriched_terms)
        most_common_terms = Counter(enriched_terms).most_common()

        self.log.debug("Total number of enriched words: %s" % (terms_count))
        unique_enriched_terms = len(set(enriched_terms))

        self.log.debug("Number of unique enriched words: %s" % (unique_enriched_terms))

        unique_terms_boost = OrderedDict()
        for occ_tuple in most_common_terms:
            unique_term = occ_tuple[0]
            unique_term_count = occ_tuple[1]
            # How many percent the unique term represents of all the enriched terms for the occupation.
            percent_unique_term = round((unique_term_count / terms_count * 100), 2)
            self.log.debug(
                "%s: %s enriched terms (%s %%)"
                % (unique_term, unique_term_count, percent_unique_term)
            )
            mean = unique_enriched_terms / terms_count
            if percent_unique_term > mean:
                term_boost = 1 + round(multiply_value * percent_unique_term, 2)
                if unique_term in terms_tf_idf:
                    # Lower boost for common terms like "svenska", "engelska"
                    term_boost = term_boost - (
                        term_boost * (terms_tf_idf[unique_term] * 10)
                    )

                if term_boost > 0:
                    unique_terms_boost[unique_term] = term_boost
        sorted_unique_terms_boost = dict(
            sorted(unique_terms_boost.items(), key=operator.itemgetter(1), reverse=True)
        )
        return sorted_unique_terms_boost

    def limit_terms_boost(self, terms_boost, limit):
        """
        Limits the number of values that will be used in the matching query.
        :param terms_boost:
        :param limit:
        :return:
        """
        limited_terms_boost = OrderedDict()
        for counter, (key, boost) in enumerate(terms_boost.items()):
            limited_terms_boost[key] = boost
            # Limit for number of "should" in boolean opensearch queries, will throw an error if higher.
            if ((counter + 1) >= limit) or (
                (counter + 1) == OPENSEARCH_MAX_CLAUSE_COUNT - 1
            ):
                break

        return limited_terms_boost

    @cache.cache("_get_jobtitle_occupation_mappings")
    def _get_jobtitle_occupation_mappings(self):
        if not self.all_enriched_occupations:
            self.enriched_occupations_handler.get_all_enriched_occupations()

        jobtitle_occupation_mappings = {}

        # Get automatically created mappings between jobtitles and occupations.
        for (
            jobtitle,
            occupation,
        ) in self._get_automatic_jobtitle_occupation_mappings().items():
            occupation_label = jmespath.search("occupation_label", occupation)
            occupation_concept_id = jmespath.search("occupation_concept_id", occupation)
            # Only return jobtitles that has a mapping to an occupation at all.
            if occupation_label:
                jobtitle_occupation_mappings[jobtitle.lower()] = (
                    self.create_jobtitle_occupation_mapping(
                        occupation_concept_id, occupation_label
                    )
                )

        # Get the exact occupation labels and ids, that has enriched data.
        for enriched_occupation in self.all_enriched_occupations.values():
            occupation_label = jmespath.search("label", enriched_occupation)
            occupation_concept_id = jmespath.search("concept_id", enriched_occupation)

            jobtitle_occupation_mappings[occupation_label.lower()] = (
                self.create_jobtitle_occupation_mapping(
                    occupation_concept_id, occupation_label
                )
            )

        # Overwrite the previous mappings with manual ones from taxonomy/higher quality.
        manual_jobtitle_occupation_mappings = (
            self._get_manual_jobtitle_mappings_from_taxonomy()
        )

        for jobtitle, occupation in manual_jobtitle_occupation_mappings.items():
            occupation_label = jmespath.search("occupation_label", occupation)
            occupation_concept_id = jmespath.search("occupation_concept_id", occupation)
            jobtitle_occupation_mappings[jobtitle.lower()] = (
                self.create_jobtitle_occupation_mapping(
                    occupation_concept_id, occupation_label
                )
            )

        # Overwrite the previous mappings with manual ones/highest quality.
        for (
            jobtitle,
            occupation,
        ) in self._get_manual_jobtitle_occupation_mappings().items():
            occupation_label = jmespath.search("occupation_label", occupation)
            occupation_concept_id = jmespath.search("occupation_concept_id", occupation)
            jobtitle_occupation_mappings[jobtitle.lower()] = (
                self.create_jobtitle_occupation_mapping(
                    occupation_concept_id, occupation_label
                )
            )

        # Remove all occupations that can't be found as enriched occupations.
        self._remove_non_existing_occupations(jobtitle_occupation_mappings)

        sorted_all_jobtitles_and_occupations = {
            k: jobtitle_occupation_mappings[k]
            for k in sorted(jobtitle_occupation_mappings)
        }

        return sorted_all_jobtitles_and_occupations

    def _remove_non_existing_occupations(self, jobtitle_occupation_mappings):
        if not self.all_enriched_occupations:
            self.enriched_occupations_handler.get_all_enriched_occupations()

        enriched_occupations_concept_ids = set(
            [
                enriched_occupation["concept_id"]
                for enriched_occupation in self.all_enriched_occupations.values()
            ]
        )
        jobtitles_and_occupations_to_remove = {}
        for jobtitle, occupation in jobtitle_occupation_mappings.items():
            if (
                occupation["occupation_concept_id"]
                not in enriched_occupations_concept_ids
            ):
                jobtitles_and_occupations_to_remove[jobtitle] = occupation
        for jobtitle, occupation in jobtitles_and_occupations_to_remove.items():
            jobtitle_occupation_mappings.pop(jobtitle, None)

    def _get_mapped_occupation_for_jobtitle(self, jobtitle):
        if not self.jobtitle_occupation_mappings:
            self.jobtitle_occupation_mappings = self._get_jobtitle_occupation_mappings()

        jobtitle_lower = jobtitle.lower()
        """
        Filter out occupations that lead to unexpected search
        results for education
        """
        df_blacklist = self._load_occupations_blacklist_items()

        if jobtitle_lower in self.jobtitle_occupation_mappings:
            mapped_occupation_id = self.jobtitle_occupation_mappings[jobtitle_lower][
                "occupation_concept_id"
            ]
            is_mapped_occupation_blacklisted = (
                df_blacklist["occupation_concept_id"]
                .str.contains(mapped_occupation_id)
                .any()
            )
            if not is_mapped_occupation_blacklisted:
                return self.jobtitle_occupation_mappings[jobtitle_lower]
            else:
                return None
        else:
            return None

    def _load_json_file(self, filepath):
        print("Loading json from file: %s" % filepath)
        with open(filepath, "r", encoding="utf-8") as file:
            data = json.load(file)
            return data

    @cache.cache("_get_manual_jobtitle_mappings_from_taxonomy")
    def _get_manual_jobtitle_mappings_from_taxonomy(self):
        data_url = settings.JOBTECH_DATA_OCCUPATIONS_URL
        self.log.info("Calling Jobtech Data URL: %s" % data_url)
        json_response = requests.get(data_url)
        json_response.raise_for_status()
        taxonomy_occupations_data = json.loads(json_response.text)
        return self.ingest_taxonomy_data(taxonomy_occupations_data)

    def ingest_taxonomy_data(self, taxonomy_occupations_data):
        if not self.synonym_dictionary:
            self.synonym_dictionary = SynonymDictionary()

        synonym_terms = self.synonym_dictionary.get_terms_by_type("YRKE")
        taxonomy_occupations = jmespath.search(
            "data.concepts[*]", taxonomy_occupations_data
        )
        self.log.info(
            f"Fetched {len(taxonomy_occupations)} occupation-names from Taxonomy"
        )
        synonym_terms_jobtitles = set(
            [synonym_term_obj["term"] for synonym_term_obj in synonym_terms]
        )
        jobtitle_occupation_mappings = {}
        for taxonomy_occupation in taxonomy_occupations:
            alt_labels = jmespath.search("alternative_labels", taxonomy_occupation)
            alt_labels_lower = [alt_label.lower() for alt_label in alt_labels]
            if alt_labels_lower:
                rel_occupation_label = jmespath.search(
                    "preferred_label", taxonomy_occupation
                )
                rel_occupation_concept_id = jmespath.search("id", taxonomy_occupation)
                for alt_label in alt_labels_lower:
                    if alt_label in synonym_terms_jobtitles:
                        jobtitle_occupation_mappings[alt_label] = {
                            "jobtitle": alt_label,
                            "occupation_label": rel_occupation_label,
                            "occupation_concept_id": rel_occupation_concept_id,
                            "percent_for_jobtitle": 0.0,
                            "occupation_enriched_ads_count": -1,
                        }
        return jobtitle_occupation_mappings

    def _get_automatic_jobtitle_occupation_mappings(self):
        filepath_mappings = FILEPATH_AUTOMATIC_JOBTITLE_OCCUPATION_MAPPINGS
        df = pd.read_csv(
            filepath_mappings,
            usecols=["jobtitle", "occupation_label", "occupation_concept_id"],
            sep=";",
            keep_default_na=False,
        )
        mappingslist = df.to_dict(orient="records")
        mappings_dict = {}
        for item in mappingslist:
            if item["occupation_concept_id"]:
                mappings_dict[item["jobtitle"]] = (
                    self.create_jobtitle_occupation_mapping(
                        item["occupation_concept_id"], item["occupation_label"]
                    )
                )

        return mappings_dict

    def _get_manual_jobtitle_occupation_mappings(self):
        filepath_mappings = FILEPATH_MANUAL_JOBTITLE_OCCUPATION_MAPPINGS
        df = pd.read_csv(
            filepath_mappings,
            usecols=["jobtitle", "occupation_label", "occupation_concept_id"],
            sep=";",
            keep_default_na=False,
        )
        mappingslist = df.to_dict(orient="records")
        mappings_dict = {}
        for item in mappingslist:
            if item["occupation_concept_id"]:
                mappings_dict[item["jobtitle"]] = (
                    self.create_jobtitle_occupation_mapping(
                        item["occupation_concept_id"], item["occupation_label"]
                    )
                )

        return mappings_dict

    def create_jobtitle_occupation_mapping(
        self, occupation_concept_id, occupation_label
    ):
        mapping = {
            "occupation_concept_id": occupation_concept_id,
            "occupation_label": occupation_label,
        }
        occupation_group = self._get_occupation_group_by_occupation_concept_id(
            occupation_concept_id
        )
        mapping["occupation_group"] = occupation_group
        return mapping

    def _get_all_valid_autocomplete_terms_and_occupation_ids(self):
        return self._get_jobtitle_occupation_mappings()

    def _get_occupation_group_by_occupation_concept_id(self, occupation_concept_id):
        if not self.all_enriched_occupations:
            self.all_enriched_occupations = (
                self.enriched_occupations_handler.get_all_enriched_occupations()
            )

        if occupation_concept_id in self.all_enriched_occupations:
            occupation = self.all_enriched_occupations[occupation_concept_id]
            occupation_group = {
                "occupation_group_label": jmespath.search(
                    "occupation_group.label", occupation
                ),
                "concept_taxonomy_id": jmespath.search(
                    "occupation_group.concept_id", occupation
                ),
                "ssyk": jmespath.search(
                    "occupation_group.legacy_ams_taxonomy_id", occupation
                ),
            }
        else:
            occupation_group = {
                "occupation_group_label": "",
                "concept_taxonomy_id": "",
                "ssyk": "",
            }
        return occupation_group
