import logging
import os
from os.path import isfile

import jmespath
from educationapi.common.opensearch_store import OpensearchStore
from opensearchpy.exceptions import *

from educationapi import settings
from educationapi.common.helper_functions import *
from educationapi.common.jobad_enrichments_client import JobAdEnrichmentsClient


SWEDISH_CONCEPT_TYPE_OCCUPATION = "YRKE"
SWEDISH_CONCEPT_TYPE_COMPETENCE = "KOMPETENS"

CANDIDATE_TYPE_OCCUPATIONS = "occupations"

OPENSEARCH_MAX_RESULT_WINDOW = 10000

currentdir = os.path.dirname(os.path.realpath(__file__)) + "/"


class OccupationsMatcher(object):
    DEFAULT_RESULT_LIMIT = 10
    DEFAULT_RESULT_OFFSET = 0

    MINIMUM_COMPETENCIES_INPUT_THRESHOLD = 3
    # Minimum amount of enriched ads for the occupation, to show in result.
    MINIMUM_ENRICHED_ADS_FOR_OCCUPATION_COUNT = 15

    synonym_dictionary = None
    enriched_occupations_handler = None

    def __init__(self, os_client, enriched_occupations_handler):
        self.log = logging.getLogger(__name__)
        self.log.info("Initializing OccupationsMatcher")

        self.os_client = os_client
        self.enriched_occupations_handler = enriched_occupations_handler

        self.opensearch_store = None
        self.enriched_occupations_alias = settings.ES_ENRICHED_OCCUPATIONS_ALIAS
        self.jobad_enrichments_client = JobAdEnrichmentsClient()
        self.blacklist_input_terms_dict = self.load_blacklist_input_terms()
        self.blacklist_result_terms_dict = self.load_blacklist_result_terms()

        self.all_enriched_occupations = None
        self.enriched_ads_total_count = None

    def get_occupation_details(self, occupation_id, include_metadata=False):
        """
        Gets an enriched occupation with for example enriched competencies and jobtitles.
        :param occupation_id: unique taxonomy conceptId
        :param include_metadata: True if metadata should be returned
        """
        if not self.all_enriched_occupations:
            self.all_enriched_occupations = (
                self.enriched_occupations_handler.get_all_enriched_occupations()
            )

        if not occupation_id or occupation_id not in self.all_enriched_occupations:
            return None

        enriched_occupation = (
            self.enriched_occupations_handler.find_occupation_by_concept_id(
                occupation_id
            )
        )
        occupation_result_object = self.populate_occupation_details_object(
            occupation_id,
            enriched_occupation,
            include_metadata,
            include_enriched_candidates_term_frequency=True,
        )

        return occupation_result_object

    def populate_occupation_details_object(
        self,
        occupation_id,
        enriched_occupation,
        include_metadata,
        include_enriched_candidates_term_frequency=False,
    ):
        """
        Populates the result object for and enriched occupation.
        """
        if not self.enriched_ads_total_count:
            self.enriched_ads_total_count = (
                self.enriched_occupations_handler.get_enriched_ads_total_count()
            )

        occupation_result_object = {
            "id": occupation_id,
            "occupation_label": jmespath.search("label", enriched_occupation),
            "concept_taxonomy_id": jmespath.search("concept_id", enriched_occupation),
            "legacy_ams_taxonomy_id": jmespath.search(
                "legacy_ams_taxonomy_id", enriched_occupation
            ),
            "occupation_group": {
                "occupation_group_label": jmespath.search(
                    "occupation_group.label", enriched_occupation
                ),
                "concept_taxonomy_id": jmespath.search(
                    "occupation_group.concept_id", enriched_occupation
                ),
                "ssyk": jmespath.search(
                    "occupation_group.legacy_ams_taxonomy_id", enriched_occupation
                ),
            },
        }
        if include_metadata:
            enriched_candidates = jmespath.search(
                "enriched_candidates", enriched_occupation
            )

            enriched_ads_count = jmespath.search(
                "enriched_ads_count", enriched_occupation
            )

            occupation_result_object["metadata"] = {
                "enriched_ads_count": enriched_ads_count,
                "enriched_ads_total_count": self.enriched_ads_total_count,
                "enriched_ads_percent_of_total": (
                    enriched_ads_count / self.enriched_ads_total_count
                )
                * 100,
            }
            if include_enriched_candidates_term_frequency:
                enriched_occupation["enriched_candidates_term_frequency"] = {}
                for candidate_type in enriched_candidates.keys():
                    enriched_for_type = jmespath.search(
                        candidate_type, enriched_candidates
                    )
                    term_frequency_for_type = (
                        self.enriched_occupations_handler.calculate_term_frequency(
                            enriched_for_type
                        )
                    )
                    enriched_occupation["enriched_candidates_term_frequency"][
                        candidate_type
                    ] = term_frequency_for_type

                occupation_result_object["metadata"][
                    "enriched_candidates_term_frequency"
                ] = enriched_occupation["enriched_candidates_term_frequency"]

        return occupation_result_object

    def match_occupations_by_id(
        self,
        education_id,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
        include_metadata=False,
    ):
        """
        Matches related occupations by unique education_id (susa-navet id)
        :param education_id: Unique susa-navet id.
        :param limit: Maximum number of hits
        :param offset: Start result from index
        :param include_metadata: True if metadata should be included
        """
        if not education_id:
            return None

        susa_education = self._get_education_by_id(education_id)

        if not susa_education:
            return None

        self.log.debug("Found education with id: %s" % education_id)

        return self.match_occupation_by_susa_education(
            susa_education, limit, offset, include_metadata
        )

    def match_occupations_by_education_code(
        self,
        education_code,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
        include_metadata=False,
    ):
        if not education_code:
            return None

        susa_education = self._get_education_by_education_code(education_code)

        if not susa_education:
            return None

        self.log.debug("Found education with education_code: %s" % education_code)
        return self.match_occupation_by_susa_education(
            susa_education, limit, offset, include_metadata
        )

    def match_occupation_by_susa_education(
        self,
        susa_education,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
        include_metadata=False,
    ):
        enriched_output_candidates = (
            self.create_input_for_match_occupation_by_susa_education(susa_education)
        )

        competence_headline_concept_terms = jmespath.search(
            "candidates_headline.competencies", enriched_output_candidates
        )
        competence_text_concept_terms = jmespath.search(
            "candidates_text.competencies", enriched_output_candidates
        )
        occupation_headline_concept_terms = jmespath.search(
            "candidates_headline.occupations", enriched_output_candidates
        )
        occupation_text_concept_terms = jmespath.search(
            "candidates_text.occupations", enriched_output_candidates
        )
        all_occupation_concept_terms = (
            occupation_headline_concept_terms + occupation_text_concept_terms
        )

        return self.match_occupations(
            competence_text_concept_terms,
            all_occupation_concept_terms,
            unwanted_occupations_regex=[],
            competencies_headline=competence_headline_concept_terms,
            include_metadata=include_metadata,
            limit=limit,
            offset=offset,
        )

    def create_input_for_match_occupation_by_susa_education(self, susa_education):
        input_headline = self.get_doc_headline_input(susa_education)
        input_text = self.get_doc_description_input(susa_education)

        return self.create_input_from_education_text(input_headline, input_text)

    def get_doc_headline_input(self, susa_education):
        sep = " | "
        headline_values = []

        education_title = jmespath.search(
            "education.title[?lang=='swe'].content | [0]", susa_education
        )
        if education_title:
            headline_values.append(education_title)

        education_plan_title = jmespath.search("education_plan.title", susa_education)
        if education_plan_title:
            headline_values.append(education_plan_title)

        education_plan_courses = jmespath.search(
            "education_plan.courses[*]", susa_education
        )
        if education_plan_courses:
            for education_plan_course in education_plan_courses:
                course_title = jmespath.search("course.title", education_plan_course)
                if course_title:
                    headline_values.append(course_title)

                education_subject_labels = jmespath.search(
                    "course.meta.educationSubjects[*].label", education_plan_course
                )
                if education_subject_labels:
                    headline_values.extend(education_subject_labels)
                new_keywords_words = jmespath.search(
                    "course.meta.nyaKeywords[0].words[*]", education_plan_course
                )
                if new_keywords_words:
                    headline_values.extend(new_keywords_words)

        events = jmespath.search("events", susa_education)
        if events:
            for event in events:
                if "extension" in event:
                    if "keywords" in event["extension"]:
                        keywords_swe = jmespath.search(
                            "extension.keywords[?lang=='swe'].content", event
                        )
                        if keywords_swe:
                            # Some keywords are numeric, like: 2030
                            keywords_swe = [str(keyword) for keyword in keywords_swe]
                            self.log.debug("keywords_swe: %s" % keywords_swe)
                            headline_values.extend(keywords_swe)

        headline_extracted_occupations = []
        for headline_value in headline_values:
            headline_extracted_occupations.append(
                self._extract_occupations_from_education_title(headline_value)
            )

        headline_values.extend(headline_extracted_occupations)

        doc_headline_input = sep.join(set(headline_values))

        self.log.debug("doc_headline_input: %s" % doc_headline_input)

        return doc_headline_input

    def get_doc_description_input(self, susa_education):
        sep = "\n"
        description_values = []

        education_description = jmespath.search(
            "education.description[?lang=='swe'].content | [0]", susa_education
        )
        if education_description:
            description_values.append(education_description)

        education_plan_brief = jmespath.search("education_plan.brief", susa_education)
        if education_plan_brief:
            description_values.append(education_plan_brief)

        education_plan_description = jmespath.search(
            "education_plan.description", susa_education
        )
        if education_plan_description:
            description_values.append(education_plan_description)

        education_plan_details = jmespath.search(
            "education_plan.details", susa_education
        )
        if education_plan_details:
            description_values.append(education_plan_details)

        education_plan_courses = jmespath.search(
            "education_plan.courses[*]", susa_education
        )
        if education_plan_courses:
            for education_plan_course in education_plan_courses:
                course_description = jmespath.search(
                    "course.description", education_plan_course
                )
                if course_description:
                    description_values.append(course_description)

        if description_values:
            doc_description_input = sep.join(description_values)
        else:
            doc_description_input = ""

        self.log.debug("doc_description_input: %s" % doc_description_input)

        return doc_description_input

    def match_occupations_by_text(
        self,
        input_text,
        input_headline="",
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
        include_metadata=False,
    ):
        enriched_output_candidates = self.create_input_from_education_text(
            input_headline, input_text
        )

        competence_headline_concept_terms = jmespath.search(
            "candidates_headline.competencies", enriched_output_candidates
        )
        competence_concept_terms = jmespath.search(
            "candidates_text.competencies", enriched_output_candidates
        )
        occupation_headline_concept_terms = jmespath.search(
            "candidates_headline.occupations", enriched_output_candidates
        )
        occupation_concept_terms = jmespath.search(
            "candidates_text.occupations", enriched_output_candidates
        )
        all_occupation_concept_terms = (
            occupation_headline_concept_terms + occupation_concept_terms
        )

        return self.match_occupations(
            competence_concept_terms,
            all_occupation_concept_terms,
            competencies_headline=competence_headline_concept_terms,
            include_metadata=include_metadata,
            limit=limit,
            offset=offset,
        )

    def create_input_from_education_text(self, input_headline, input_text):
        occupations_thresh = float(settings.ENRICH_EDUCATIONS_OCCUPATIONS_THRESHOLD)
        extracted_input_headline = self._extract_occupations_from_education_title(
            input_headline
        )
        # Enrich all text...
        enriched_results = self.jobad_enrichments_client.enrich_textdocument(
            "", extracted_input_headline, input_text
        )
        enriched_results_candidates = jmespath.search(
            "[0].enriched_candidates", enriched_results
        )

        enriched_output_candidates_text = self.get_empty_output_value()[
            "enriched_candidates"
        ]
        enriched_output_candidates_headline = self.get_empty_output_value()[
            "enriched_candidates"
        ]

        for attr_name in enriched_results_candidates.keys():
            enriched_candidates_for_type = jmespath.search(
                attr_name, enriched_results_candidates
            )

            if attr_name == CANDIDATE_TYPE_OCCUPATIONS:
                # Add occupations from the headline, even if they are below prediction threshold.
                filtered_candidates_headline_for_type = [
                    candidate
                    for candidate in enriched_candidates_for_type
                    if candidate["sentence_index"] == 0
                ]

                # Only add occupations if they have a prediction above threshold.
                filtered_candidates_text_for_type = [
                    candidate
                    for candidate in enriched_candidates_for_type
                    if candidate["prediction"] > occupations_thresh
                    and candidate["sentence_index"] > 0
                ]

            else:
                filtered_candidates_headline_for_type = [
                    candidate
                    for candidate in enriched_candidates_for_type
                    if candidate["sentence_index"] == 0
                ]
                filtered_candidates_text_for_type = [
                    candidate
                    for candidate in enriched_candidates_for_type
                    if candidate["sentence_index"] > 0
                ]

            unique_candidates_headline_for_type = self.format_unique_concept_labels(
                filtered_candidates_headline_for_type
            )
            cleaned_candidates_headline = [
                term
                for term in unique_candidates_headline_for_type
                if term not in self.blacklist_input_terms_dict[attr_name]
            ]

            unique_candidates_text_for_type = self.format_unique_concept_labels(
                filtered_candidates_text_for_type
            )
            cleaned_candidates_text = [
                term
                for term in unique_candidates_text_for_type
                if term not in self.blacklist_input_terms_dict[attr_name]
                and term not in unique_candidates_headline_for_type
            ]

            enriched_output_candidates_headline[attr_name] = cleaned_candidates_headline
            enriched_output_candidates_text[attr_name] = cleaned_candidates_text

        return_dict = {
            "candidates_headline": enriched_output_candidates_headline,
            "candidates_text": enriched_output_candidates_text,
        }
        return return_dict

    # Extract education words (not terms) from education title.
    # Return a list of extracted words from title.
    def _extract_occupations_from_education_title(self, education_title):
        if not education_title:
            return ""

        new_title = education_title

        cleaned_title = education_title.strip()
        cleaned_title = cleaned_title.lower()
        replaced_title_words = cleaned_title
        # Remove as suffix, e.g. tandläkarprogrammet -> tandläkar
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "programmet", ""
        )
        # Remove as suffix, e.g. humanistprogram -> humanist
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "program", ""
        )
        # Remove as suffix, e.g. flygteknikutbildningen  -> flygteknik
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "utbildningen", ""
        )
        # Remove as suffix, e.g. flygteknikutbildning  -> flygteknik
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "utbildning", ""
        )
        # Remove as suffix, e.g. itkursen  -> it
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "kursen", ""
        )
        # Remove as suffix, e.g. itkurser  -> it
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "kurser", ""
        )
        # Remove as suffix, e.g. itkurs  -> it
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "kurs", ""
        )
        # Remove as suffix, e.g. Musikteaterlinjen -> musikteater
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "linjen", ""
        )
        # Remove as suffix, e.g. Musikteaterlinje -> musikteater
        replaced_title_words = self._replace_suffix_from_str(
            replaced_title_words, "linje", ""
        )

        if replaced_title_words != cleaned_title:
            # Only rewrite the remainder if the suffix has been rewritten already.

            # Remove as suffix, e.g. läkar -> läkare
            replaced_title_words = self._replace_suffix_from_str(
                replaced_title_words, "ar", "are"
            )
            # Remove as suffix, e.g. sjuksköterske -> sjuksköterska
            replaced_title_words = self._replace_suffix_from_str(
                replaced_title_words, "ske", "ska"
            )
            # Remove as suffix, e.g. vallning -> vallare
            replaced_title_words = self._replace_suffix_from_str(
                replaced_title_words, "ning", "are"
            )
            # Remove as suffix, e.g. grävmaskin -> grävmaskinist
            replaced_title_words = self._replace_suffix_from_str(
                replaced_title_words, "maskin", "maskinist"
            )
            # Remove as suffix, e.g. fysioterapi -> fysioterapeut
            replaced_title_words = self._replace_suffix_from_str(
                replaced_title_words, "terapi", "terapeut"
            )
            # Remove as suffix, e.g. maskinoperation -> maskinoperatör
            replaced_title_words = self._replace_suffix_from_str(
                replaced_title_words, "tion", "tör"
            )

        if replaced_title_words != cleaned_title:
            new_title = replaced_title_words

        return new_title

    def _replace_suffix_from_str(self, str, replace_suffix, new_suffix):
        if not str or str == "":
            return str

        if str.endswith(replace_suffix):
            pos = str.rfind(replace_suffix)
            if pos > -1:
                str = str[:pos] + new_suffix + str[pos + len(replace_suffix) :]
        return str

    def load_blacklist_input_terms(self):
        candidates_type_names = ["competencies", "occupations", "traits", "geos"]
        blacklist_terms_dict = {}
        for type_name in candidates_type_names:
            blacklist_filepath = (
                f"{currentdir}../resources/blacklist_input_{type_name}.txt"
            )
            blacklist_terms = set(self.load_blacklist_file(blacklist_filepath))
            blacklist_terms_dict[type_name] = blacklist_terms
        return blacklist_terms_dict

    def load_blacklist_result_terms(self):
        candidates_type_names = ["competencies", "occupations", "traits", "geos"]
        blacklist_terms_dict = {}
        for type_name in candidates_type_names:
            blacklist_filepath = (
                f"{currentdir}../resources/blacklist_result_{type_name}.txt"
            )
            blacklist_terms = set(self.load_blacklist_file(blacklist_filepath))
            blacklist_terms_dict[type_name] = blacklist_terms
        return blacklist_terms_dict

    def load_blacklist_file(self, blacklist_filepath):
        if not isfile(blacklist_filepath):
            return []

        with open(blacklist_filepath, encoding="utf-8") as stoplistfile:
            blacklist_terms = stoplistfile.readlines()
        blacklist_terms = [x.strip() for x in blacklist_terms]
        return blacklist_terms

    def get_empty_output_value(self):
        return {
            "enriched_candidates": {
                "occupations": [],
                "competencies": [],
                "traits": [],
                "geos": [],
            }
        }

    def format_unique_concept_labels(self, enriched_candidates):
        labels = [
            candidate["concept_label"].lower() for candidate in enriched_candidates
        ]
        labels = list(set(labels))
        return sorted(labels)

    def _create_es_match_occupations_query(
        self,
        competencies=None,
        occupations=None,
        unwanted_occupations_regex=None,
        competencies_headline=None,
    ):
        if competencies is None:
            competencies = []

        if occupations is None:
            occupations = []

        if unwanted_occupations_regex is None:
            unwanted_occupations_regex = []

        # Add occupation labels that never should appear in result.
        for term in self.blacklist_result_terms_dict["occupations"]:
            unwanted_occupations_regex.append(term)

        if competencies_headline is None:
            competencies_headline = []

        query = {"query": {"bool": {"should": [], "must": [], "must_not": []}}}

        should_node = jmespath.search("query.bool.should", query)
        must_node = jmespath.search("query.bool.must", query)
        must_not_node = jmespath.search("query.bool.must_not", query)
        competencies = format_input_terms(competencies)

        for competence in competencies:
            phrase_query = {
                "match_phrase": {
                    # f"occupation.relevant_enriched_candidates.competencies": {
                    "occupation.enriched_candidates.competencies": {"query": competence}
                }
            }
            should_node.append(phrase_query)

        competencies_headline = format_input_terms(competencies_headline)

        for competence in competencies_headline:
            phrase_query = {
                "match_phrase": {
                    # f"occupation.relevant_enriched_candidates.competencies": {
                    "occupation.enriched_candidates.competencies": {
                        "query": competence,
                        "boost": 50,
                    }
                }
            }
            should_node.append(phrase_query)

        occupations_bool_node = {"bool": {"should": [], "minimum_should_match": 1}}

        if occupations:
            occupations_should_node = jmespath.search(
                "bool.should", occupations_bool_node
            )

            occupations = format_input_terms(occupations)

            for occupation in occupations:
                phrase_query = {
                    "match_phrase": {
                        "occupation.relevant_enriched_candidates.occupations": {
                            # f"occupation.enriched_candidates.occupations": {
                            "query": occupation,
                            "boost": 10,
                        }
                    }
                }
                occupations_should_node.append(phrase_query)
            must_node.append(occupations_bool_node)

        minimum_enriched_ads_count_range_query = {
            "range": {
                "occupation.enriched_ads_count": {
                    "gte": OccupationsMatcher.MINIMUM_ENRICHED_ADS_FOR_OCCUPATION_COUNT
                }
            }
        }
        must_node.append(minimum_enriched_ads_count_range_query)

        if unwanted_occupations_regex:
            for unwanted_occupation_regex in unwanted_occupations_regex:
                regex_query = {
                    "regexp": {"occupation.label.keyword": unwanted_occupation_regex}
                }
                must_not_node.append(regex_query)

        # import json
        # self.log.info(json.dumps(query, ensure_ascii=False, indent=4))
        return query

    def match_occupations(
        self,
        competencies_concept_terms,
        occupations_concept_terms,
        unwanted_occupations_regex=None,
        competencies_headline=None,
        include_metadata=False,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
    ):
        """
        Main method for matching occupations by competence terms and occupation (jobtitle) terms.
        :param unwanted_occupations_regex:
        """
        if not competencies_headline:
            competencies_headline = []

        all_competencies_concept_terms = (
            competencies_concept_terms + competencies_headline
        )

        if not occupations_concept_terms:
            if not all_competencies_concept_terms or self.is_too_few_competencies_input(
                all_competencies_concept_terms
            ):
                return self.create_empty_result_object(
                    all_competencies_concept_terms, occupations_concept_terms
                )

        result_object = self.do_matching_query_and_create_result(
            competencies_concept_terms,
            occupations_concept_terms,
            unwanted_occupations_regex=unwanted_occupations_regex,
            competencies_headline=competencies_headline,
            include_metadata=include_metadata,
            limit=limit,
            offset=offset,
        )

        if jmespath.search("hits_total", result_object) > 0:
            return result_object
        else:
            empty_occupations_concept_terms = []
            # The previous query returned zero hits, try to query a 2nd time without the input occupations_concept_terms
            # since they can cause zero hits.
            if not competencies_concept_terms or self.is_too_few_competencies_input(
                competencies_concept_terms
            ):
                return self.create_empty_result_object(
                    all_competencies_concept_terms, empty_occupations_concept_terms
                )

            result_object_plan_b = self.do_matching_query_and_create_result(
                competencies_concept_terms,
                empty_occupations_concept_terms,
                unwanted_occupations_regex=unwanted_occupations_regex,
                competencies_headline=competencies_headline,
                include_metadata=include_metadata,
                limit=limit,
                offset=offset,
            )
            return result_object_plan_b

    def is_too_few_competencies_input(self, competencies_concept_terms):
        return (
            len(competencies_concept_terms)
            < OccupationsMatcher.MINIMUM_COMPETENCIES_INPUT_THRESHOLD
        )

    def do_matching_query_and_create_result(
        self,
        competencies_concept_terms,
        occupations_concept_terms,
        unwanted_occupations_regex=None,
        competencies_headline=None,
        include_metadata=False,
        limit=DEFAULT_RESULT_LIMIT,
        offset=DEFAULT_RESULT_OFFSET,
    ):
        limit, offset = self._adjust_limit_and_offset(limit, offset)
        query = self._create_es_match_occupations_query(
            competencies=competencies_concept_terms,
            occupations=occupations_concept_terms,
            unwanted_occupations_regex=unwanted_occupations_regex,
            competencies_headline=competencies_headline,
        )
        alias_name = self.enriched_occupations_alias

        source_excludes_fields = [
            "occupation.enriched_candidates.*",
            "occupation.relevant_enriched_candidates.*",
        ]
        # import json
        # self.log.info(f'query: {json.dumps(query)}')
        try:
            matched_occupations_results = self.os_client.search(
                index=alias_name,
                body=query,
                search_type="dfs_query_then_fetch",
                size=limit,
                from_=offset,
                track_total_hits=True,
                _source_excludes=source_excludes_fields,
                request_timeout=60,
            )

            hits_total = jmespath.search(
                "hits.total.value", matched_occupations_results
            )

            hits = jmespath.search("hits.hits", matched_occupations_results)
            result_object = {
                "hits_total": hits_total,
                "hits_returned": len(hits),
                "identified_keywords_for_input": {
                    "competencies": competencies_headline + competencies_concept_terms,
                    "occupations": occupations_concept_terms,
                },
                "related_occupations": [],
            }

            for counter, hit in enumerate(hits):
                match_score = jmespath.search("_score", hit)
                occupation_hit = jmespath.search("_source.occupation", hit)
                # self.log.info(json.dumps(occupation_hit, ensure_ascii=False, indent=4))
                occupation_id = jmespath.search("_source.id", hit)

                occupation_result_object = self.populate_occupation_details_object(
                    occupation_id, occupation_hit, include_metadata
                )

                if include_metadata:
                    occupation_result_object["metadata"]["match_score"] = match_score

                result_object["related_occupations"].append(occupation_result_object)

            # self.log.info(json.dumps(result_object, ensure_ascii=False))
        except NotFoundError as e:
            self.log.error(
                f"_match_occupations(). Can't find index {alias_name} and matching won't work: {e}"
            )
            return None

        return result_object

    def create_empty_result_object(
        self, identified_competencies=None, identified_occupations=None
    ):
        if not identified_competencies:
            identified_competencies = []
        if not identified_occupations:
            identified_occupations = []

        result_object = {
            "hits_total": 0,
            "hits_returned": 0,
            "identified_keywords_for_input": {
                "competencies": identified_competencies,
                "occupations": identified_occupations,
            },
            "related_occupations": [],
        }
        return result_object

    def _adjust_limit_and_offset(self, limit, offset):
        if limit and limit > OPENSEARCH_MAX_RESULT_WINDOW:
            limit = OPENSEARCH_MAX_RESULT_WINDOW
        if not limit or limit <= 0:
            limit = self.DEFAULT_RESULT_LIMIT
        if not offset or offset < 0:
            offset = self.DEFAULT_RESULT_OFFSET
        return limit, offset

    def _get_education_by_id(self, education_id):
        if not self.opensearch_store:
            self.opensearch_store = OpensearchStore(self.os_client)

        return self.opensearch_store.find_by_id(education_id)

    def _get_education_by_education_code(self, education_code):
        query = {"query": {"match": {"education.code": education_code}}}
        educations_alias_name = settings.ES_ENRICHED_EDUCATIONS_ALIAS

        educations_source_excludes_fields = ["events.*", "education_providers.*"]

        education_by_code_result = self.os_client.search(
            index=educations_alias_name,
            body=query,
            search_type="dfs_query_then_fetch",
            size=1,
            track_total_hits=True,
            _source_excludes=educations_source_excludes_fields,
        )

        first_hit = jmespath.search("hits.hits[0]._source", education_by_code_result)

        return first_hit
