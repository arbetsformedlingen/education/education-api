import json
import logging

import requests
from flashtext.keyword import KeywordProcessor

from educationapi import settings


class SynonymDictionary(object):
    all_jae_terms = None

    handled_term_types = ["COMPETENCE", "OCCUPATION"]

    def __init__(self, stoplist=None):
        self.log = logging.getLogger(__name__)

        self.jae_api_host_url = settings.JAE_API_URL

        if stoplist is None:
            stoplist = []
        self.stoplist = stoplist

        self.include_misspelled = True

        self.concept_to_term = {}
        self.keyword_processor = KeywordProcessor()
        self.init_keyword_processor(self.keyword_processor)
        self.init_synonym_dictionary(self.keyword_processor)

    def __len__(self):
        return len(self.get_keyword_processor())

    def misspelled_predicate(self, value, include_misspelled):
        if not include_misspelled and value["term_misspelled"]:
            return False
        return True

    def get_filtered_synonym_dictionary_terms(self):
        return (
            termobj
            for termobj in self.fetch_jae_terms()
            if termobj["term"] not in self.stoplist
        )

    def init_synonym_dictionary(self, keyword_processor):
        for term_obj in self.get_filtered_synonym_dictionary_terms():
            keyword_processor.add_keyword(term_obj["term"], term_obj)
            concept_preferred_label = term_obj["concept"].lower()
            if concept_preferred_label not in self.concept_to_term:
                self.concept_to_term[concept_preferred_label] = []
            self.concept_to_term[concept_preferred_label].append(term_obj)

    @staticmethod
    def init_keyword_processor(keyword_processor):
        [keyword_processor.add_non_word_boundary(token) for token in list("åäöÅÄÖ()-*")]

    def get_keyword_processor(self):
        return self.keyword_processor

    def get_concepts(
        self, text, concept_type=None, include_misspelled=True, span_info=False
    ):
        concepts = self.get_keyword_processor().extract_keywords(
            text, span_info=span_info
        )
        if concept_type is not None:
            if span_info:
                concepts = list(
                    filter(lambda concept: concept[0]["type"] == concept_type, concepts)
                )
            else:
                concepts = list(
                    filter(lambda concept: concept["type"] == concept_type, concepts)
                )

        concepts = [
            termobj
            for termobj in concepts
            if self.misspelled_predicate(termobj, include_misspelled)
        ]

        return concepts

    def right_replace(self, s, old, new):
        occurrence = 1
        li = s.rsplit(old, occurrence)
        return new.join(li)

    def get_all_terms(self):
        return self.all_jae_terms

    def get_terms_by_type(self, term_type):
        return [
            term_obj
            for term_obj in self.all_jae_terms
            if self.all_jae_terms and term_obj["type"] == term_type
        ]

    def fetch_jae_terms(self):
        if self.all_jae_terms:
            for term in self.all_jae_terms:
                yield term

        spelling_type = "BOTH"

        headers = {"accept": "application/json"}

        all_terms = []
        for type_name in self.handled_term_types:
            url_jae_api = "%s/synonymdictionary?type=%s&spelling=%s" % (
                self.jae_api_host_url,
                type_name,
                spelling_type,
            )
            self.log.info("Calling JAE API URL: %s" % url_jae_api)
            json_response = requests.get(url_jae_api, headers=headers)
            json_response.raise_for_status()
            result = json.loads(json_response.text)
            if "items" in result:
                all_terms.extend(result["items"])

        self.all_jae_terms = all_terms

        for term in all_terms:
            yield term


# synonym_dictionary = SynonymDictionary()
# all_terms = synonym_dictionary.get_all_terms()
# self.log.info('len, all_terms: %s' % len(all_terms))
# self.log.info(synonym_dictionary.get_concepts('vi söker någon som är nogrann och kan java', include_misspelled=True))
# self.log.info(synonym_dictionary.get_concepts('nogrann flexibel snabb', include_misspelled=True))
# self.log.info(synonym_dictionary.get_concepts('java python ekonomi', include_misspelled=True))
