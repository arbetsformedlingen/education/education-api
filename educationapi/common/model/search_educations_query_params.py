class SearchEducationsQueryParams(object):
    def __init__(self):
        self._free_text = None
        self._education_type = None
        self._education_form = None
        self._municipality_code = None
        self._region_code = None
        self._pace_of_study_percentage = None
        self._education_code = None
        self._distance = None
        self._filter_education_plan_exists = None

    @property
    def free_text(self):
        # Getter
        return self._free_text

    @free_text.setter
    def free_text(self, free_text):
        # setter
        self._free_text = free_text


    @property
    def education_type(self):
        # Getter
        return self._education_type

    @education_type.setter
    def education_type(self, education_type):
        # setter
        self._education_type = education_type

    @property
    def education_form(self):
        # Getter
        return self._education_form

    @education_form.setter
    def education_form(self, education_form):
        # setter
        self._education_form = education_form


    @property
    def municipality_code(self):
        # Getter
        return self._municipality_code

    @municipality_code.setter
    def municipality_code(self, municipality_code):
        # setter
        self._municipality_code = municipality_code

    @property
    def region_code(self):
        # Getter
        return self._region_code

    @region_code.setter
    def region_code(self, region_code):
        # setter
        self._region_code = region_code


    @property
    def pace_of_study_percentage(self):
        # Getter
        return self._pace_of_study_percentage

    @pace_of_study_percentage.setter
    def pace_of_study_percentage(self, pace_of_study_percentage):
        # setter
        self._pace_of_study_percentage = pace_of_study_percentage


    @property
    def education_code(self):
        # Getter
        return self._education_code

    @education_code.setter
    def education_code(self, education_code):
        # setter
        self._education_code = education_code

    @property
    def distance(self):
        # Getter
        return self._distance

    @distance.setter
    def distance(self, distance):
        # setter
        self._distance = distance

    @property
    def filter_education_plan_exists(self):
        # Getter
        return self._filter_education_plan_exists

    @filter_education_plan_exists.setter
    def filter_education_plan_exists(self, filter_education_plan_exists):
        # setter
        self._filter_education_plan_exists = filter_education_plan_exists

# c = C()
# c.x = 'foo'  # setter called
# foo = c.x    # getter called