import logging
import sys
import time

import requests

from educationapi import settings

ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
ENRICHER_RETRIES = 10


class JobAdEnrichmentsClient(object):

    def __init__(self):
        self.log = logging.getLogger(__name__)
        self.jae_api_host_url = settings.JAE_API_URL
        self.jae_api_endpoint_url = self.jae_api_host_url + '/enrichtextdocuments'
        self.log.info(f'Will enrich at endpoint {self.jae_api_endpoint_url}')

    def enrich_textdocument(self, doc_id, doc_headline, doc_text):

        json_input = {
            "include_terms_info": True,
            "include_sentences": True,
            "sort_by_prediction_score": "NOT_SORTED"
        }
        json_input["documents_input"] = [{
            ENRICHER_PARAM_DOC_ID: doc_id,
            ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
            ENRICHER_PARAM_DOC_TEXT: doc_text
        }]

        enriched_doc = self.get_enrich_result(json_input)

        return enriched_doc

    def get_enrich_result(self, json_input, timeout=60):

        headers = {'Content-Type': 'application/json'}

        for retry in range(ENRICHER_RETRIES):
            try:
                r = requests.post(url=self.jae_api_endpoint_url, headers=headers, json=json_input, timeout=timeout)
                r.raise_for_status()
            except Exception as e:
                self.log.info(f"get_enrich_result() retrying #{retry + 1} after error: {e}")
                time.sleep(0.5)
            else:
                return r.json()
        self.log.error(f"_get_enrich_result failed after: {ENRICHER_RETRIES} retries with error. Exit!")
        sys.exit(1)
