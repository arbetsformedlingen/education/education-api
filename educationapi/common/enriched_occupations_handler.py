import json
import logging
import os
from collections import Counter

import jmespath
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options
from opensearchpy.exceptions import *
from opensearchpy.helpers import scan

from educationapi import settings

currentdir = os.path.dirname(os.path.realpath(__file__)) + "/"


class EnrichedOccupationsHandler(object):
    cache_opts = {
        "cache.type": "memory",
        "cache.expire": settings.ENRICHED_OCCUPATIONS_CACHE_EXPIRE_SECONDS,
    }

    cache = CacheManager(**parse_cache_config_options(cache_opts))

    enriched_ads_total_count = None
    _all_enriched_occupations = None

    def __init__(self, os_client):
        self.log = logging.getLogger(__name__)
        self.enriched_occupations_alias = settings.ES_ENRICHED_OCCUPATIONS_ALIAS
        self.os_client = os_client

    def get_all_enriched_occupations(self):
        if self._all_enriched_occupations:
            return self._all_enriched_occupations

        query_dsl = {
            "_source": {
                "excludes": [
                    "occupation.enriched_candidates.*",
                    "occupation.relevant_enriched_candidates.*",
                ]
            },
            "query": {"match_all": {}},
        }

        try:
            search_result = scan(
                self.os_client,
                query_dsl,
                index=self.enriched_occupations_alias,
                size=1000,
                request_timeout=60,
            )

            all_enriched_occupations = {}
            for result_item in search_result:
                enriched_occupation_id = jmespath.search("_source.id", result_item)
                # One of the items has id == None.
                if enriched_occupation_id:
                    enriched_occupation = jmespath.search(
                        "_source.occupation", result_item
                    )
                    all_enriched_occupations[enriched_occupation_id] = (
                        enriched_occupation
                    )
            self._all_enriched_occupations = all_enriched_occupations

        except NotFoundError as e:
            self.log.error(
                f"_fetch_all_enriched_occupations(). Can't find index {self.enriched_occupations_alias} and matching won't work: {e}"
            )
            return {}

        return self._all_enriched_occupations

    def _load_json_file(self, filepath):
        self.log.debug("Loading json from file: %s" % filepath)
        with open(filepath, "r", encoding="utf-8") as file:
            data = json.load(file)
            return data

    @cache.cache("get_terms_tf_idf")
    def get_terms_tf_idf(self):
        filepath = currentdir + "../resources/enriched_occupations_tf_idf_mean.json"
        self.log.info(f"get_terms_tf_idf - loading {filepath}")
        return self._load_json_file(filepath)

    def get_enriched_ads_total_count(self):
        if self.enriched_ads_total_count:
            return self.enriched_ads_total_count

        enriched_ads_total_count = 0
        for occupation in self.get_all_enriched_occupations().values():
            enriched_ads_total_count += jmespath.search(
                "enriched_ads_count", occupation
            )

        self.enriched_ads_total_count = enriched_ads_total_count

        return self.enriched_ads_total_count

    def get_percent_for_jobtitle_in_occupation(
        self, occupation_taxonomy_concept_id, jobtitle
    ):
        percent_for_jobtitle_in_occupation = 0
        taxonomy_occupation = self.find_occupation_by_concept_id(
            occupation_taxonomy_concept_id
        )

        if taxonomy_occupation:
            enriched_candidates = jmespath.search(
                "enriched_candidates", taxonomy_occupation
            )
            taxonomy_occupation["enriched_candidates_term_frequency"] = {}
            for candidate_type in enriched_candidates.keys():
                enriched_for_type = jmespath.search(candidate_type, enriched_candidates)
                term_frequency_for_type = self.calculate_term_frequency(
                    enriched_for_type
                )
                taxonomy_occupation["enriched_candidates_term_frequency"][
                    candidate_type
                ] = term_frequency_for_type

            enriched_occupations_term_frequency = jmespath.search(
                "enriched_candidates_term_frequency.occupations", taxonomy_occupation
            )
            jobtitle_obj = None
            for item in enriched_occupations_term_frequency:
                if item["term"] == jobtitle:
                    jobtitle_obj = item
                    break

            if jobtitle_obj:
                percent_for_jobtitle_in_occupation = jmespath.search(
                    "percent_for_occupation", jobtitle_obj
                )
        return percent_for_jobtitle_in_occupation

    @cache.cache("find_occupation_by_concept_id")
    def find_occupation_by_concept_id(self, occupation_taxonomy_concept_id):
        query_dsl = {
            "query": {
                "match": {"occupation.concept_id": occupation_taxonomy_concept_id}
            }
        }
        search_result = self.os_client.search(
            query_dsl, self.enriched_occupations_alias
        )

        hits_total_value = jmespath.search("hits.total.value", search_result)
        if hits_total_value > 0:
            fetched_occupation = jmespath.search(
                "hits.hits[0]._source.occupation", search_result
            )
            return fetched_occupation
        else:
            return None

    def calculate_term_frequency(self, enriched_terms):
        if not enriched_terms:
            return {}
        terms_count = len(enriched_terms)
        most_common_terms = Counter(enriched_terms).most_common()

        self.log.debug("Total number of enriched words: %s" % (terms_count))
        unique_enriched_terms = len(set(enriched_terms))
        self.log.debug("Number of unique enriched words: %s" % (unique_enriched_terms))

        terms_and_percent = []
        for occ_tuple in most_common_terms:
            unique_term = occ_tuple[0]
            unique_term_count = occ_tuple[1]

            percent_unique_term = (unique_term_count / terms_count) * 100
            terms_and_percent.append(
                {"term": unique_term, "percent_for_occupation": percent_unique_term}
            )

        return terms_and_percent
