import json
import logging

import jmespath
import requests
from beaker.cache import CacheManager
from beaker.util import parse_cache_config_options

from educationapi import settings


class SearchparamValues(object):
    cache_opts = {
        "cache.type": "memory",
        "cache.expire": settings.SEARCHPARAM_VALUES_CACHE_EXPIRE_SECONDS,
    }

    cache = CacheManager(**parse_cache_config_options(cache_opts))

    def __init__(self, os_client):
        self.log = logging.getLogger(__name__)
        self.os_client = os_client
        self.enriched_educations_alias = settings.ES_ENRICHED_EDUCATIONS_ALIAS

    @cache.cache("get_education_forms")
    def get_education_forms(self):
        opensearch_field = "education.form.code.keyword"
        aggregations_values = self._get_aggregations_values(opensearch_field)
        return self._prepare_aggregations_result(aggregations_values)

    @cache.cache("get_education_types")
    def get_education_types(self):
        opensearch_field = "education.configuration.code.keyword"
        aggregations_values = self._get_aggregations_values(opensearch_field)
        return self._prepare_aggregations_result(aggregations_values)

    @cache.cache("get_municipality_codes")
    def get_municipality_codes(self):
        opensearch_field = "events.locations.municipalityCode.keyword"
        educations_municipality_codes = self._get_aggregations_values(opensearch_field)
        taxonomy_municipalities = self._get_municipalities_from_taxonomy()

        used_municipality_codes = []

        for municipality_code in educations_municipality_codes:
            if municipality_code in taxonomy_municipalities:
                used_municipality_codes.append(
                    taxonomy_municipalities[municipality_code]
                )

        sorted_used_municipality_codes = sorted(
            used_municipality_codes, key=lambda d: d["value"]
        )

        self.log.info(
            f"Municipality codes - nr in education data: {len(educations_municipality_codes)}, "
            f"nr in taxonomy: {len(taxonomy_municipalities)}, "
            f"nr in both: {len(used_municipality_codes)}"
        )

        return sorted_used_municipality_codes

    @cache.cache("_get_municipalities_from_taxonomy")
    def _get_municipalities_from_taxonomy(self):
        # https://taxonomy.api.jobtechdev.se/v1/taxonomy/specific/concepts/municipality?include-legacy-information=false
        tax_host_url = settings.TAXONOMY_API_HOST_URL

        tax_api_url = f"{tax_host_url}/v1/taxonomy/specific/concepts/municipality?include-legacy-information=false"
        self.log.info("Calling Taxonomy API URL: %s" % tax_api_url)
        json_response = requests.get(tax_api_url)

        json_response.raise_for_status()
        json_text = json.loads(json_response.text)

        self.log.info("Fetched %s municipalities from Taxonomy API" % (len(json_text)))
        tax_municipalities = {}
        for municipality_obj in json_text:
            tax_municipalities[municipality_obj["taxonomy/lau-2-code-2015"]] = {
                "key": municipality_obj["taxonomy/lau-2-code-2015"],
                "value": municipality_obj["taxonomy/preferred-label"],
            }

        return tax_municipalities

    @cache.cache("get_region_codes")
    def get_region_codes(self):
        opensearch_field = "events.locations.regionCode.keyword"
        educations_region_codes = self._get_aggregations_values(opensearch_field)
        taxonomy_regions = self._get_regions_from_taxonomy()

        used_region_codes = []

        for region_code in educations_region_codes:
            if region_code in taxonomy_regions:
                used_region_codes.append(taxonomy_regions[region_code])

        sorted_used_municipality_codes = sorted(
            used_region_codes, key=lambda d: d["value"]
        )

        self.log.info(
            f"Region codes - nr in education data: {len(educations_region_codes)}, "
            f"nr in taxonomy: {len(taxonomy_regions)}, "
            f"nr in both: {len(used_region_codes)}"
        )

        return sorted_used_municipality_codes

    @cache.cache("_get_regions_from_taxonomy")
    def _get_regions_from_taxonomy(self):
        # https://taxonomy.api.jobtechdev.se/v1/taxonomy/specific/concepts/region?include-legacy-information=false
        tax_host_url = settings.TAXONOMY_API_HOST_URL

        tax_api_url = f"{tax_host_url}/v1/taxonomy/specific/concepts/region?include-legacy-information=false"
        self.log.info("Calling Taxonomy API URL: %s" % tax_api_url)
        json_response = requests.get(tax_api_url)

        json_response.raise_for_status()
        json_data = json.loads(json_response.text)

        self.log.info("Fetched %s regions from Taxonomy API" % (len(json_data)))
        tax_regions = {}
        for region_obj in json_data:
            # Filter on national-nuts-level-3-code-2019 since not all regions are Swedish län.
            # Only Swedish län regions contain a national-nuts-level-3-code-2019.
            if region_obj.get("taxonomy/national-nuts-level-3-code-2019", False):
                tax_regions[region_obj["taxonomy/national-nuts-level-3-code-2019"]] = {
                    "key": region_obj["taxonomy/national-nuts-level-3-code-2019"],
                    "value": region_obj["taxonomy/preferred-label"],
                }

        return tax_regions

    def _prepare_aggregations_result(self, aggregations_values):
        result_list = []
        for aggregation_value in aggregations_values:
            result_list.append(
                {"key": aggregation_value, "value": aggregation_value.capitalize()}
            )
        return result_list

    def _get_aggregations_values(self, opensearch_field):
        aggs_query = {
            "size": 0,
            "aggs": {
                "education_values": {"terms": {"field": opensearch_field, "size": 1000}}
            },
        }
        aggs_result = self.os_client.search(aggs_query, self.enriched_educations_alias)
        education_keys = jmespath.search(
            "aggregations.education_values.buckets[*].key", aggs_result
        )
        if education_keys:
            education_keys = sorted(education_keys)
        return education_keys
