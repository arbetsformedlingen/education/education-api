import logging

from opensearchpy import OpenSearch

from educationapi import settings

log = logging.getLogger(__name__)


def create_os_client():

    hosts = [{"host": host, "port": settings.ES_PORT} for host in settings.ES_HOST]
    log.info(f"Using Opensearch host(s): {hosts}")

    client = OpenSearch(
        hosts=hosts,
        http_compress=True,  # enables gzip compression for request bodies
        http_auth=(settings.ES_USER, settings.ES_PWD),
        use_ssl=settings.ES_USE_SSL,
        verify_certs=settings.ES_VERIFY_CERTS,
        ssl_assert_hostname=False,
        ssl_show_warn=False,
    )

    return client
