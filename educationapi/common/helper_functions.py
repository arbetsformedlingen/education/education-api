def format_input_terms(input_terms):
    '''
    Formats terms for searching for example terms that contains hyphens.
    '''
    formatted_terms = []
    for term in input_terms:
        formatted_term = format_input_term(term)

        formatted_terms.append(formatted_term)
    return formatted_terms

def format_input_term(term):
    # Replace hyphens with underscore since hyphens has been replaced when indexing in Opensearch,
    # to make it possible to search for "HR-assistent". Without the replacement Opensearch will
    # remove the hyphen according to "HR assistent" and "HR-assistent" won't be found.
    return term.replace('-', '_')
