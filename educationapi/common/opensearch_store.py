import itertools
import json
import time
import logging
from datetime import datetime
from opensearchpy.exceptions import NotFoundError
from educationapi import settings
from educationapi.common.model.search_educations_query_params import (
    SearchEducationsQueryParams,
)
from educationapi.common.pagination_params import PaginationParams


class OpensearchStore:
    def __init__(self, os_client):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)
        self.os_client = os_client

    def _grouper(self, n, iterable):
        iterable = iter(iterable)
        return iter(lambda: list(itertools.islice(iterable, n)), [])

    def find_by_id(self, id_value):
        index = settings.ES_EDUCATIONS_ALIAS
        query_dsl = {"query": {"ids": {"values": [id_value]}}}
        return self.find(query_dsl, index)

    # OpenSearch has a limit of max 10000 docs in result
    def full_search(
        self, pagination_params: PaginationParams, exclude_events_and_providers=True
    ):
        index = settings.ES_EDUCATIONS_ALIAS
        query_dsl = {
            "query": {"match_all": {}},
            "track_total_hits": True,
            "track_scores": True,
        }
        if exclude_events_and_providers:
            query_dsl["_source"] = {"exclude": ["events", "education_providers"]}

        if pagination_params.limit:
            query_dsl["size"] = pagination_params["limit"]
        else:
            query_dsl["size"] = 2000
        if pagination_params.offset:
            query_dsl["from"] = pagination_params.offset
        self.log.debug(f"Query DSL: {query_dsl}")
        return self.search(query_dsl, index)

    # Returns all docs in the index using scan operator.
    def full_scan_search(self, exclude_events_and_providers=True):
        index = settings.ES_EDUCATIONS_ALIAS
        query_dsl = {
            "query": {"match_all": {}},
            "track_total_hits": True,
            "track_scores": True,
        }
        if exclude_events_and_providers:
            query_dsl["_source"] = {"exclude": ["events", "education_providers"]}
        self.log.debug(f"Query DSL: {query_dsl}")
        scan_result = self.os_client.scan_search(query_dsl, index)
        counter = 0
        yield "["
        for result_doc in scan_result:
            if counter > 0:
                yield ","
            yield json.dumps(result_doc)
            counter += 1
        self.log.info(f"(full_scan_search)Delivered: {counter} educations")
        yield "]"

    def freetext_search(
        self,
        search_educations_query_params: SearchEducationsQueryParams,
        pagination_params: PaginationParams,
        exclude_events_and_providers,
    ):
        index = settings.ES_EDUCATIONS_ALIAS
        query_dsl = {
            "query": {"bool": {}},
            "track_total_hits": True,
            "track_scores": True,
        }
        # Free text search...
        # Search title fields using regex (free text string as prefix and anything as suffix). This will cover a lot of grammar).
        # Search various fields (e.g. subject, keywords, education plan and enriched candidates) using low boost to include
        # educations that could be relevant directly or indirectly.
        if search_educations_query_params.free_text:
            free_text = search_educations_query_params.free_text
            query_dsl["query"]["bool"]["should"] = [
                {"match": {"id": {"query": free_text, "boost": 10}}},
                {
                    "regexp": {
                        "education.title.content": {
                            "value": free_text + ".*",
                            "boost": 10,
                        }
                    }
                },
                {
                    "match": {
                        "education.description.content": {
                            "query": free_text,
                            "boost": 2,
                        }
                    }
                },
                {"match": {"education.subject.name": {"query": free_text, "boost": 1}}},
                {
                    "match": {
                        "education.subject.nameEn": {"query": free_text, "boost": 1}
                    }
                },
                {
                    "match": {
                        "events.extension.keywords.content": {
                            "query": free_text,
                            "boost": 1,
                        }
                    }
                },
                {
                    "regexp": {
                        "education_plan.program_title": {
                            "value": free_text + ".*",
                            "boost": 5,
                        }
                    }
                },
                {
                    "match": {
                        "education_plan.description": {"query": free_text, "boost": 2}
                    }
                },
                {
                    "regexp": {
                        "education_plan.courses.course_title": {
                            "value": free_text + ".*",
                            "boost": 3,
                        }
                    }
                },
                {
                    "match": {
                        "education_plan.courses.description": {
                            "query": free_text,
                            "boost": 1,
                        }
                    }
                },
                {
                    "match": {
                        "text_enrichments_results.enriched_candidates.occupations": {
                            "query": free_text,
                            "boost": 2,
                        }
                    }
                },
                {
                    "match": {
                        "text_enrichments_results.enriched_candidates.competencies": {
                            "query": free_text,
                            "boost": 2,
                        }
                    }
                },
                {
                    "match": {
                        "text_enrichments_results.enriched_candidates.traits": {
                            "query": free_text,
                            "boost": 1,
                        }
                    }
                },
                {
                    "match": {
                        "text_enrichments_results.enriched_candidates.geos": {
                            "query": free_text,
                            "boost": 1,
                        }
                    }
                },
            ]
            query_dsl["query"]["bool"]["minimum_should_match"] = 1
            query_dsl["query"]["bool"]["boost"] = 1.0

        must_list = []

        # Education type filter (e.g. 'program' or 'kurs'):
        if search_educations_query_params.education_type:
            education_type_query = self._create_multiple_terms_query(
                search_educations_query_params.education_type,
                "education.configuration.code.keyword",
            )
            must_list.append(education_type_query)
        # Ecucation form filter (e.g. 'högskoleutbildning' or 'gymnasieskola':
        if search_educations_query_params.education_form:
            education_type_query = self._create_multiple_terms_query(
                search_educations_query_params.education_form,
                "education.form.code.keyword",
            )
            must_list.append(education_type_query)

        if search_educations_query_params.municipality_code:
            must_list.append(
                {
                    "term": {
                        "events.locations.municipalityCode": search_educations_query_params.municipality_code
                    }
                }
            )

        if search_educations_query_params.region_code:
            must_list.append(
                {
                    "term": {
                        "events.locations.regionCode": search_educations_query_params.region_code
                    }
                }
            )

        if search_educations_query_params.pace_of_study_percentage:
            must_list.append(
                {
                    "term": {
                        "events.paceOfStudyPercentage": search_educations_query_params.pace_of_study_percentage
                    }
                }
            )
        if search_educations_query_params.education_code:
            must_list.append(
                {
                    "term": {
                        "education.code.keyword": search_educations_query_params.education_code
                    }
                }
            )
        if search_educations_query_params.filter_education_plan_exists:
            must_list.append({"exists": {"field": "education_plan"}})
        if search_educations_query_params.distance:
            must_list.append({"term": {"eventSummary.distance": True}})

        if len(must_list) > 0:
            query_dsl["query"]["bool"]["must"] = must_list

        if pagination_params.limit:
            query_dsl["size"] = pagination_params.limit
        else:
            query_dsl["size"] = 10000
        if pagination_params.offset:
            query_dsl["from"] = pagination_params.offset
        if exclude_events_and_providers:
            query_dsl["_source"] = {"exclude": ["events", "education_providers"]}
        self.log.debug(f"Query DSL: {json.dumps(query_dsl)}")
        return self.search(query_dsl, index)

    def _create_multiple_terms_query(self, query_term_list, search_field_name):
        boolean_node = {"bool": {"should": []}}
        boolean_should_node = boolean_node["bool"]["should"]
        for query_term in query_term_list:
            boolean_should_node.append(
                {"term": {search_field_name: query_term.lower()}}
            )
        return boolean_node

    # Search where exactly one hit is expected (e.g search by id)...
    def find(self, query_dsl, index):
        query_result = self.os_client.search(query_dsl, index)
        formatted_result = None
        if query_result.get("hits", None):
            hits = query_result["hits"]
            if hits.get("hits", None):
                hits_result = hits["hits"]
                if len(hits_result) > 0:
                    formatted_result = {}
                    doc = hits_result[0]
                    if doc.get("_source", None):
                        formatted_result = doc["_source"]
        return formatted_result

    # Search where zero or more hits are expected...
    def search(self, query_dsl, index):
        query_result = self.os_client.search(query_dsl, index)
        total_value = 0
        formatted_result = None
        if query_result.get("hits", None):
            hits = query_result["hits"]
            if hits.get("total", None):
                total = hits["total"]
                if total.get("value", None):
                    total_value = total["value"]
            if hits.get("hits", None):
                hits_result = hits["hits"]
                if len(hits_result) > 0:
                    formatted_result = []
                    for doc in hits_result:
                        if doc.get("_source", None):
                            formatted_result.append(doc["_source"])

        result = {"hits": total_value, "result": formatted_result}
        return result
